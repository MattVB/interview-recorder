//
//  IRUiControlHelper.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 29/07/12.
//
//

#import <QuartzCore/QuartzCore.h>
#import "IRUiControlHelper.h"

@implementation IRUiControlHelper

+(void)formatTextView:(UITextView *)textView
{
    [textView.layer setBackgroundColor: [[UIColor whiteColor] CGColor]];
    [textView.layer setBorderColor: [[UIColor grayColor] CGColor]];
    [textView.layer setBorderWidth: 1.0];
    [textView.layer setCornerRadius:8.0f];
    [textView.layer setMasksToBounds:YES];
}

@end
