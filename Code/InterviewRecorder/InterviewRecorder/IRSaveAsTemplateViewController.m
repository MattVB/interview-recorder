//
//  IRSaveAsTemplateViewController.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 17/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IRSaveAsTemplateViewController.h"

@interface IRSaveAsTemplateViewController ()

@end

@implementation IRSaveAsTemplateViewController
@synthesize textTemplateName;
@synthesize delegate = _delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Put the text focus to the template name field
    [textTemplateName becomeFirstResponder];
}

- (void)viewDidUnload
{
    [self setTextTemplateName:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (IBAction)save:(id)sender 
{
    // Call the delegate
    [self.delegate saveAsTemplateControllerDone:self templateName:textTemplateName.text];
}
@end
