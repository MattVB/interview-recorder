//
//  IRQuestionResponseAudioPlayer.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 1/08/12.
//
//

#import <Foundation/Foundation.h>
#import "IRResponse.h"
#import "IRQuestionResponse.h"

@interface IRQuestionResponseAudioPlayer : NSObject

@property (weak, nonatomic) IRResponse* response;
@property (weak, nonatomic) IRQuestionResponse* questionResponse;


    -(id)initWithResponse:(IRResponse*)inputResponse andQuestion:(IRQuestionResponse*)inputQuestionResponse;

    -(BOOL)hasAudioToPlay;

    -(void)playAudio;

    -(void)stopAudio;

@end
