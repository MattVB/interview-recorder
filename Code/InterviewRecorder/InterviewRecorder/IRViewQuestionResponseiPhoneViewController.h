//
//  IRAnalyseViewQuestionResponseiPhoneViewController.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 18/08/12.
//
//

#import <UIKit/UIKit.h>
#import "IRResponse.h"
#import "IRQuestionResponse.h"

@interface IRViewQuestionResponseiPhoneViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *labelRespondent;
@property (weak, nonatomic) IBOutlet UILabel *labelQuestionText;
@property (weak, nonatomic) IBOutlet UITextView *textViewResponse;
@property (weak, nonatomic) IBOutlet UIButton *buttonPlayAudio;
- (IBAction)playAudio:(id)sender;

@property (weak, nonatomic) IRResponse *response;
@property (weak, nonatomic) IRQuestionResponse *questionResponse;

@end
