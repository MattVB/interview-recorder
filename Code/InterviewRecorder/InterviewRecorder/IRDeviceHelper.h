//
//  IRDeviceHelper.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 15/08/12.
//
//

#import <Foundation/Foundation.h>

@interface IRDeviceHelper : NSObject

+(BOOL)isIpad;

+(void)dismissPopoverOrPopFromController:(UIViewController*) viewController withPopoverController:(UIPopoverController*) popoverController;

+(BOOL)shouldAutoRotatePortaitOnlyForPhone:(UIInterfaceOrientation)interfaceOrientation;

@end
