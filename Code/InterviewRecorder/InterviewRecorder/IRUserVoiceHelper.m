//
//  IRUserVoiceHelper.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 26/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IRUserVoiceHelper.h"

#import "UserVoice.h"

@implementation IRUserVoiceHelper

+ (void)presentUserVoiceModalViewControllerForParent:(UIViewController *)viewController
{
    [UserVoice presentUserVoiceModalViewControllerForParent:viewController
                                                    andSite:@"mattsapps.uservoice.com"
                                                     andKey:@"Kaz7ElmPDrHDZ9XKVzw"
                                                  andSecret:@"aN5qZvtidEf267yR8DzmyhnzQ2epzCJUp82rMikgIc"];
}

@end

