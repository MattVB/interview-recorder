//
//  IRAudioResponseSegment.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 21/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IRAudioResponseSegment : NSObject <NSCoding>

    @property (nonatomic) NSTimeInterval startTime;

    @property (nonatomic) NSTimeInterval endTime;

    -(id)initWithStartTime:(NSTimeInterval)inputStartTime endTime:(NSTimeInterval)inputEndTime;

@end
