//
//  IRInterviewAudioPlayerViewController.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 21/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IRInterviewAudioPlayer.h"

@interface IRInterviewAudioPlayerViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *labelCurrentlyPlaying;
@property (weak, nonatomic) IBOutlet UIButton *buttonPlayPause;
- (IBAction)playPause:(id)sender;
@property (weak, nonatomic) IBOutlet UISlider *sliderCurrentPosition;

@property (weak, nonatomic) IRInterviewAudioPlayer *audioPlayer;
- (IBAction)currentPositionValueChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *buttonPrevious;
- (IBAction)previous:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *buttonNext;
- (IBAction)next:(id)sender;

@end
