//
//  IRResponsesListViewController.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 15/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IRResponsesListViewController.h"
#import "IRResponse.h"
#import "IRResponseDataController.h"
#import "IRViewResponseViewController.h"
#import "IRRecordResponseViewController.h"
#import "IRQuestionResponse.h"
#import "IRMaintainTemplatesListViewController.h"
#import "IREditTemplateViewController.h"
#import "IRUserVoiceHelper.h"
#import "IRGroupingHelper.h"
#import "IRDeviceHelper.h"

@interface IRResponsesListViewController ()

@property UIPopoverController *popoverController;
@property IRTemplate *templateForInterview;
@property IRTemplate *templateToOpen;
@property IRGroupingHelper *groupingHelper;
@property IRMaintainTemplatesListViewController *maintainTemplatesViewController;

@end

@implementation IRResponsesListViewController
@synthesize barButtonItemActions = _barButtonItemActions;
@synthesize buttonAnalyseReponses;

@synthesize popoverController = __popoverController;
@synthesize templateForInterview = _templateForInterview;
@synthesize  templateToOpen = _templateToOpen;
@synthesize groupingHelper = _groupingHelper;
@synthesize maintainTemplatesViewController = _maintainTemplatesViewController;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    // Create the grouping helper
    [self createGroupingHelper];
}

- (void)viewDidUnload
{
    [self setButtonAnalyseReponses:nil];
    [self setBarButtonItemActions:nil];
    [super viewDidUnload];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

-(void)createGroupingHelper
{
    IRResponseDataController *dataController = [[IRResponseDataController alloc]init];
    NSMutableArray *responses = [dataController getResponses];
    
    self.groupingHelper = [[IRGroupingHelper alloc]initWithResponses:responses];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    NSInteger groupingsCount = [self.groupingHelper getGroupings].count;
    
    if (groupingsCount == 0)
    {
        // If there are no responses then return 1 for the helper cell
        return 1;
    }
    
    return groupingsCount;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([self.groupingHelper getGroupings].count == 0)
    {
        return @"";
    }
    
    return [[self.groupingHelper getGroupings] objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([self.groupingHelper getGroupings].count == 0)
    {
        return 1;
    }
    
    // Return the number of rows in the section.
    NSString *grouping = [[self.groupingHelper getGroupings] objectAtIndex:section];
    return [self.groupingHelper getResponseCountForGrouping:grouping];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.groupingHelper getGroupings].count == 0)
    {
        // Create the NoResponses cell.
        return [tableView dequeueReusableCellWithIdentifier:@"NoResponsesCell"];
    }
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    NSString *grouping = [[self.groupingHelper getGroupings] objectAtIndex:indexPath.section];
    NSMutableArray *responses = [self.groupingHelper getResponsesWithGrouping:grouping];
    IRResponse *response = [responses objectAtIndex:indexPath.row];
    
    // Populate the cell
    cell.textLabel.text = [response getDescription];   
    cell.detailTextLabel.text = [response getResponseSummary];
    
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.groupingHelper getGroupings].count == 0)
    {
        return 150;
    }
    
    // Default height for a table cell
    return 44;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        // Determine the response to be deleted
        NSString *grouping = [[self.groupingHelper getGroupings] objectAtIndex:indexPath.section];
        NSMutableArray *responses = [self.groupingHelper getResponsesWithGrouping:grouping];
        IRResponse *response = [responses objectAtIndex:indexPath.row];
        
        // Delete the response
        IRResponseDataController *responseDataController = [[IRResponseDataController alloc]init];
        [responseDataController deleteResponse:response];
        
        // Update the grouping helper
        [self createGroupingHelper];
        
        // Update the table view. If deleting the last item in a section then delete the section, otherwise delete the
        // table row.
        if (responses.count == 1)
        {
            if ([self.groupingHelper getGroupings].count > 0)
            {
                // Delete the section
                [tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
            }
            else
            {
                [tableView reloadData];
            }
        }
        else
        {
            // Delete the row from the data source
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
        
        if ([self.groupingHelper getGroupings].count == 0)
        {
            // If no more interviews then add a row for the info row
            //[tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }

    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

#pragma mark - Table view delegate


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the destination view controller
    id destinationViewController = [segue destinationViewController];
    
    // If this is a navigation view controller then drill through to the first view controller to be actually displayed
    if ([destinationViewController isKindOfClass:[UINavigationController class]])
    {
        destinationViewController = [[destinationViewController viewControllers] objectAtIndex:0];
    }
    
    // Check for if this is a popover and if so - hold onto the popover controller so that we can dismiss later
    if ([segue isKindOfClass:[UIStoryboardPopoverSegue class]])
    {
        self.popoverController = ((UIStoryboardPopoverSegue*)segue).popoverController;        
    }
    
    if ([[segue identifier] isEqualToString:@"ShowResponse"]) {
        // Determine the response to be displayed
         NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSString *grouping = [[self.groupingHelper getGroupings] objectAtIndex:indexPath.section];
        NSMutableArray *responses = [self.groupingHelper getResponsesWithGrouping:grouping];
        IRResponse *response = [responses objectAtIndex:indexPath.row];
        
        // Set the response on the new controller
        IRViewResponseViewController *newController = (IRViewResponseViewController *)destinationViewController;
        newController.response = response;
    }
    
    if ([[segue identifier] isEqualToString:@"StartInterview"]) {
        // Get the controller and set the delegate
        IRRecordResponseViewController *newController = (IRRecordResponseViewController *)destinationViewController;
        newController.delegate = self;
        
        // Create the response
        IRResponse *response = [[IRResponse alloc]initResponse];
        response.dateResponded = [NSDate date];
        response.respondent = @"";
        response.grouping = self.templateForInterview.templateName;
        
        // Create the questions
        NSMutableArray *createdQuestions = [[NSMutableArray alloc]init];
        for(NSString *questionText in self.templateForInterview.questions)
        {
            [createdQuestions addObject:[[IRQuestionResponse alloc]initWithQuestion:questionText]];
        }
        
        // Pass over the selected questions
        response.questions = createdQuestions;
        
        
        // Assign to the view controller
        newController.response = response;
        newController.questionIndex = 0;
    }
    
    if ([[segue identifier] isEqualToString:@"SelectTemplate"]) {
        IRSelectTemplateViewController *newController = (IRSelectTemplateViewController *)destinationViewController;
        newController.delegate = self;
    }
    
    if ([[segue identifier] isEqualToString:@"MaintainTemplates"]) {
        IRMaintainTemplatesListViewController *newController = (IRMaintainTemplatesListViewController *)destinationViewController;        
        newController.delegate = self;
    }
    
    if ([[segue identifier] isEqualToString:@"EditTemplate"]) {
        IREditTemplateViewController *newController = (IREditTemplateViewController *)destinationViewController;
        
        // Set the delegate
        newController.delegate = self.maintainTemplatesViewController;
        
        // Set the template to edit
        newController.template = self.templateToOpen;
    }
}

-(void)selectTemplateViewControllerDidFinish:(IRSelectTemplateViewController *)controller template:(IRTemplate *)template
{
    // Hold onto the template
    self.templateForInterview = template;
    
    // Close the popover
    [self.popoverController dismissPopoverAnimated:YES];
    
    // Move onto the new segue
    [self performSegueWithIdentifier:@"StartInterview" sender:self];
}


-(void)recordResponseViewControllerComplete:(IRRecordResponseViewController *)controller response:(IRResponse *)response
{
    // Store the updated response
    IRResponseDataController *dataController = [[IRResponseDataController alloc]init];
    [dataController storeResponse:response];
    
    // Dismiss this view controller
    [self.navigationController popToViewController:self animated:YES];
    
    // Refresh the grid
    [self createGroupingHelper];
    [self.tableView reloadData];
}

-(void)recordResponseViewControllerCancelled:(IRRecordResponseViewController *)controller
{
    // Pop back to this view
    [self.navigationController popToViewController:self animated:YES];
}

-(void)maintainTemplateListViewControllerStartEditing:(IRMaintainTemplatesListViewController *)controller templateToEdit:(IRTemplate *)templateToEdit
{
    // Set the template
    self.templateToOpen = templateToEdit;
    
    // Dismiss the popover
    [self.popoverController dismissPopoverAnimated:YES];
    
    // Hold onto the maintain template controller
    self.maintainTemplatesViewController = controller;
    
    // Push to the edit screen
    [self performSegueWithIdentifier:@"EditTemplate" sender:nil];
}

-(void)maintainTemplateListViewControllerAddTemplate:(IRMaintainTemplatesListViewController *)controller
{
    // Create a new template
    self.templateToOpen = [[IRTemplate alloc]initWitTemplateName:@""];
    
    // Dismiss the popover
    [self.popoverController dismissPopoverAnimated:YES];
    
    // Hold onto the maintain template controller
    self.maintainTemplatesViewController = controller;
    
    // Push to the edit screen
    [self performSegueWithIdentifier:@"EditTemplate" sender:nil];
}

- (IBAction)provideAppFeedback:(id)sender 
{
    // Show the UserVoice screen
    [IRUserVoiceHelper presentUserVoiceModalViewControllerForParent:self];
}
- (IBAction)showActionsList:(id)sender
{
    // Create the action list
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Actions" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Maintain Templates", @"Analyse Responses", @"Provide App Feedback", nil];
    
    // Show the action list
    UIBarButtonItem *button = (UIBarButtonItem*)sender;
    [actionSheet showFromBarButtonItem:button animated:YES];
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        // Templates
        [self performSegueWithIdentifier:@"MaintainTemplates" sender:self.barButtonItemActions];
    }
    
    if(buttonIndex == 1)
    {
        // Analyse responses
        [self performSegueWithIdentifier:@"AnalyseResponses" sender:self.barButtonItemActions];
    }
    
    if(buttonIndex == 2)
    {
        // Provide app feedback
        [self provideAppFeedback:nil];
    }
}
@end
