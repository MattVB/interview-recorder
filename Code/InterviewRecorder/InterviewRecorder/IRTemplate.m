//
//  IRTemplate.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 15/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IRTemplate.h"

@implementation IRTemplate

    @synthesize templateName = _templateName;
    @synthesize questions = _questions;


-(id)initWitTemplateName:(NSString *)inputTemplateName
{
    self = [super init];
    if (self) {
        self.templateName = inputTemplateName;
        self.questions = [[NSMutableArray alloc]init];
        
        return self;
    }
    
    return nil;
}

-(void)addQuestion:(NSString *)question
{
    [self.questions addObject:question];
}

-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.templateName forKey:@"templateName"];
    [aCoder encodeObject:self.questions forKey:@"questions"];
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self.templateName = [aDecoder decodeObjectForKey:@"templateName"];
    self.questions = [aDecoder decodeObjectForKey:@"questions"];
    
    return self;
}

@end
