//
//  IRVersionsInstalledDataController.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 22/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

// Provides functionality for storing the list of versions of this application that have been installed.
// Helps for doing upgrade actions and changing hint messages displayed.
// For now is very basic - will be expanded in the future.
@interface IRVersionsInstalledDataController : NSObject

    -(void)doAppStartupActions;

@end
