//
//  IRMaintainTemplatesListViewController.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 18/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IRMaintainTemplatesListViewController.h"
#import "IRTemplateDataController.h"

@interface IRMaintainTemplatesListViewController ()

@end

@implementation IRMaintainTemplatesListViewController

@synthesize delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    IRTemplateDataController *templateDataController = [[IRTemplateDataController alloc]init];
    return [templateDataController getTemplates].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TemplateCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Get the relevant template
    IRTemplateDataController *templateDataController = [[IRTemplateDataController alloc]init];
    NSMutableArray *templates = [templateDataController getTemplates];
    IRTemplate *template = [templates objectAtIndex:indexPath.row];
    
    // Set the template name as the text label
    cell.textLabel.text = template.templateName;
    
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Find the tempate to delete
        IRTemplateDataController *templateDataController = [[IRTemplateDataController alloc]init];
        NSMutableArray *templates = [templateDataController getTemplates];
        IRTemplate *template = [templates objectAtIndex:indexPath.row];
        
        // Delete the template
        [templateDataController deleteTemplateWithName:template.templateName];
        
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Get the template
    IRTemplateDataController *templateDataController = [[IRTemplateDataController alloc]init];
    NSMutableArray *templates = [templateDataController getTemplates];
    IRTemplate *template = [templates objectAtIndex:indexPath.row];
    
    // Call the delegate to start editing
    [self.delegate maintainTemplateListViewControllerStartEditing:self templateToEdit:template];

}

- (IBAction)addTemplate:(id)sender 
{
    // Call the delegate
    [self.delegate maintainTemplateListViewControllerAddTemplate:self];
}

-(void)editTemplateControllerDone:(IREditTemplateViewController *)controller
{
    // Reload the data
    [self.tableView reloadData];
}
@end
