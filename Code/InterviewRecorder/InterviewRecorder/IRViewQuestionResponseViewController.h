//
//  IRViewQuestionResponseViewController.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 2/08/12.
//
//

#import <UIKit/UIKit.h>
#import "IRResponse.h"
#import "IRQuestionResponse.h"
#import "IRInterviewAudioPlayer.h"

@interface IRViewQuestionResponseViewController : UIViewController

@property (weak, nonatomic) IRResponse* response;
@property (weak, nonatomic) IRQuestionResponse* questionResponse;
@property (weak, nonatomic) IRInterviewAudioPlayer* audioPlayer;

@property (weak, nonatomic) IBOutlet UITextView *textViewResponse;
@property (weak, nonatomic) IBOutlet UIButton *buttonPlayAudio;
- (IBAction)playAudio:(id)sender;

@end
