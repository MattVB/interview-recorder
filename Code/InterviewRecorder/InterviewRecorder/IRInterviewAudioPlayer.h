//
//  IRInterviewAudioPlayer.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 21/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IRResponse.h"
#import "IRQuestionResponse.h"

// Helper class for playing interview audio. Only handles playing from a single interview at a time.
@interface IRInterviewAudioPlayer : NSObject

-(id)initWithResponse:(IRResponse*)inputResponse;

-(void)playInterview;

-(void)playQuestionResponse:(IRQuestionResponse*)questionResponse;

-(void)pause;

-(void)resume;

-(void)stop;

-(BOOL)playing;

-(NSTimeInterval)duration;

-(NSTimeInterval)currentTime;

-(void)skipToTime:(NSTimeInterval)timeToSkipTo;

-(IRQuestionResponse*)currentlyPlayingQuestion;

-(void)nextAudioSegment;

-(void)previousAudioSegment;

@end
