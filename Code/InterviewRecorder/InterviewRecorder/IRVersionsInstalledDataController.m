//
//  IRVersionsInstalledDataController.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 22/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IRVersionsInstalledDataController.h"

@implementation IRVersionsInstalledDataController

static NSString *versionsInstalledListKey = @"VersionsInstalledList";

-(NSString*)getCurrentAppVersion
{
    // Get the version number
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleVersionKey];
}

-(void)doAppStartupActions
{
    // Get the list
    NSMutableArray *versionsList = [self getVersionsList];
    
    if (![versionsList containsObject:[self getCurrentAppVersion]])
    {
        // Version is not in the list.
        // Add the version and save
        [versionsList addObject:[self getCurrentAppVersion]];
        [self setVersionsList:versionsList];
    }
}


-(NSMutableArray*)getVersionsList
{
    // Get the value from the UserDefaults object
    NSUserDefaults *userSettings = [NSUserDefaults standardUserDefaults];
    NSData *data = [userSettings objectForKey:versionsInstalledListKey];
    
    // If there is data then unarchive and return
    if (data)
    {
        NSArray *arr = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        return [[NSMutableArray alloc]initWithArray:arr];
    }
    
    // Create a new array and return
    return [[NSMutableArray alloc]init];
}


-(void)setVersionsList:(NSMutableArray*)versionList
{
    // Archive the data
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:versionList];
    
    // Store to the UserDefaults
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:versionsInstalledListKey];
}

@end
