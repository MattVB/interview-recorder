//
//  IREmailContentGenerator.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 23/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IREmailContentGenerator.h"
#import "IRResponse.h"
#import "IRQuestionResponse.h"

@implementation IREmailContentGenerator

-(NSString*)generateEmailSubjectForInterviewResponse:(IRResponse *)response
{
    return [NSString stringWithFormat:@"Interview: %@", [response getDescription]];
}

-(NSString*)generateEmailContentForInterviewResponse:(IRResponse *)response
{
    NSString* result = @"";
    
    // Add in each question
    for (IRQuestionResponse *question in response.questions)
    {
        result = [result stringByAppendingString:[self generateEmailContentForQuestion:question]];
    }
    
    // If there was an overall comment then add this
    if (![response.overallComments isEqualToString:@""])
    {
        result = [result stringByAppendingFormat:@"\nOverall comments:\n%@", response.overallComments];
    }
    
    return result;
}


-(NSString*)generateEmailContentForQuestion:(IRQuestionResponse*)question
{
    NSString *result = @"";
    result = [result stringByAppendingFormat:@"%@:\n%@", question.question, [question getFormattedResponse]];
    
    // If there are flgas then include these in the email
    if (question.flags.count > 0)
    {
        NSString *flagsString = @"";
        
        // Generate a string with the list of flags
        for (NSString* flag in question.flags)
        {
            if (![flagsString isEqualToString:@""])
            {
                flagsString = [flagsString stringByAppendingString:@"  "];
            }
            
            flagsString = [flagsString stringByAppendingString:flag];
        }
        
        // Add the flags onto the question on a new line.
        result = [result stringByAppendingFormat:@"\n***%@", flagsString];
    }
    
    // Add in a blank line
    result = [result stringByAppendingString:@"\n\n"];
    
    // Return the result
    return result;
}

@end
