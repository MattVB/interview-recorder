//
//  IRViewResponseViewController.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 15/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IRViewResponseViewController.h"
#import "IRQuestionResponse.h"
#import "IRViewQuestionResponseViewController.h"
#import "IRResponseFlags.h"
#import "IRAudioFileHelper.h"
#import "IRInterviewAudioPlayer.h"
#import "IRInterviewAudioPlayerViewController.h"
#import "IREmailHelper.h"
#import "IRUserVoiceHelper.h"
#import "IRDateHelper.h"
#import "IRDeviceHelper.h"
#import "IRViewQuestionResponseiPhoneViewController.h"

#import <AVFoundation/AVFoundation.h>
#import <MessageUI/MessageUI.h>

@interface IRViewResponseViewController ()
 @property IRInterviewAudioPlayer *audioPlayer;
    @property UIPopoverController *popoverController;
    @property UIBarButtonItem *actionBarButtonItem;
    @property NSMutableArray *availableActions;
@end

@implementation IRViewResponseViewController

@synthesize labelGrouping = _labelGrouping;
@synthesize response = _response;
@synthesize tableView = _tableView;
@synthesize labelRespondentName = _labelRespondentName;
@synthesize labelDate = _labelDate;
@synthesize labelInterviewerName = _labelInterviewerName;
@synthesize labelOverallComments = _labelOverallComments;
@synthesize audioButton = _audioButton;
@synthesize availableActions = _availableActions;


@synthesize audioPlayer = _audioPlayer;
@synthesize popoverController = __popoverController;
@synthesize actionBarButtonItem = _actionBarButtonItem;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Set the label values
    self.labelRespondentName.text = self.response.respondent;
    self.labelInterviewerName.text = self.response.interviewer;
    self.labelOverallComments.text = self.response.overallComments;
    self.labelGrouping.text = self.response.grouping;
    
    // Set the date label
    self.labelDate.text = [IRDateHelper formatDate:self.response.dateResponded];
    
    // Set the navigation item title
    self.navigationItem.title = [self.response getDescription];
    
    self.actionBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(actionButton)];
    self.navigationItem.rightBarButtonItem = self.actionBarButtonItem;
    
    // Create the audio player
    self.audioPlayer = [[IRInterviewAudioPlayer alloc]initWithResponse:self.response];
    
    // Check whether there was audio and hide button if not
    if (!self.response.audioFileName)
    {
        // No audio so hide the button
        self.audioButton.hidden = YES;
    }
    else 
    {
        self.audioButton.hidden = NO;
    }
}

-(void)actionButton
{
    UIActionSheet *actionSheet;
    
    self.availableActions = [[NSMutableArray alloc]init];
    
    // Always able to email
    [self.availableActions addObject:@"Email"];
    
    // If audio then include option for emailing that
    if (self.response.audioFileName)
    {
        [self.availableActions addObject:@"Email with Audio"];
    }
    
    // If on a phone then show audio option
    if (![IRDeviceHelper isIpad])
    {
        [self.availableActions addObject:@"Play Audio"];
    }
    
    // Add in the cancel option
    [self.availableActions addObject:@"Cancel"];
    
    actionSheet = [[UIActionSheet alloc]initWithTitle:@"Actions" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
    
    for (NSString* action in self.availableActions)
    {
        [actionSheet addButtonWithTitle:action];
    }
    
    actionSheet.cancelButtonIndex = self.availableActions.count - 1;
    
    /*
     // Create an action sheet - different depending on whether there is audio or not
    if (!self.response.audioFileName)
    {
        actionSheet = [[UIActionSheet alloc]initWithTitle:@"Actions" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Email", nil];
    }
    else {
        actionSheet = [[UIActionSheet alloc]initWithTitle:@"Actions" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Email", @"Email with Audio", nil];
    }
     */
    
    [actionSheet showFromBarButtonItem:self.actionBarButtonItem animated:YES];
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    NSString *action = [self.availableActions objectAtIndex:buttonIndex];
    
    // Email
    if ([action hasPrefix:@"Email"])
    {
        // Check for not able to send an email
        if (![MFMailComposeViewController canSendMail])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                            message:@"Your device doesn't support sending an email."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            return;
        }
        
        bool includeAudio = [action isEqualToString:@"Email with Audio"];
        
        // Create the email view controller
        IREmailHelper *emailHelper = [[IREmailHelper alloc]init];
        MFMailComposeViewController *mailer = [emailHelper createMailComposeViewControllerForInterviewResponse:self.response withAudio:includeAudio];
        
        // Set the delegate
        mailer.mailComposeDelegate = self;
        
        // Go to the email sending
        [self presentModalViewController:mailer animated:YES];
    }
    
    if ([action isEqualToString:@"Play Audio"])
    {
        // Go to the audio view
        [self performSegueWithIdentifier:@"PlayAudio" sender:nil];
    }
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    // Remove the mail view
    [self dismissModalViewControllerAnimated:YES];
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    if ([IRDeviceHelper isIpad])
    {
        // Stop the audio player and release
        [self.audioPlayer stop];
        self.audioPlayer = nil;
    }
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [self setLabelRespondentName:nil];
    [self setLabelDate:nil];
    [self setLabelInterviewerName:nil];
    [self setLabelOverallComments:nil];
    [self setAudioButton:nil];
    [self setLabelGrouping:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.response.questions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ResponseCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    IRQuestionResponse *question = [self.response.questions objectAtIndex:indexPath.row];
    
    // Set the question and deatil label
    cell.textLabel.text = question.question;
    cell.detailTextLabel.text = question.response;  
    
    // If there are flags then display these
    if([self.response anyQuestionsHaveFlags])
    {
        // Get the flag and display the appropriate image
        if (question.flags.count > 0)
        {
            NSString *firstFlag = [question.flags objectAtIndex:0];
            NSString *flagImageName = [IRResponseFlags getImageNameForFlag:firstFlag];
            cell.imageView.image = [UIImage imageNamed:flagImageName];  
        }
        else 
        {
            // No flags - display an empty image so that everything has the correct alignment
            cell.imageView.image = [UIImage imageNamed:@"Empty.png"];
        }
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Get the question response
    IRQuestionResponse *questionRespone = [self.response.questions objectAtIndex:indexPath.row];
    
    if ([IRDeviceHelper isIpad])
    {
        // Create the view controller
        IRViewQuestionResponseViewController *questionResponseViewController = [[IRViewQuestionResponseViewController alloc]init];
        questionResponseViewController.questionResponse = questionRespone;
        questionResponseViewController.response = self.response;
        questionResponseViewController.audioPlayer = self.audioPlayer;
        
        // Create popover and display
        self.popoverController = [[UIPopoverController alloc]
                              initWithContentViewController:questionResponseViewController];
        self.popoverController.popoverContentSize = CGSizeMake(520, 120);
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [self.popoverController presentPopoverFromRect:cell.bounds inView:cell.contentView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else
    {
        // Push the segue
        [self performSegueWithIdentifier:@"ViewQuestionResponse" sender:questionRespone];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the destination view controller
    id destinationViewController = [segue destinationViewController];
    
    // Check for if this is a popover and if so - hold onto the popover controller so that we can dismiss later
    if ([segue isKindOfClass:[UIStoryboardPopoverSegue class]])
    {
        self.popoverController = ((UIStoryboardPopoverSegue*)segue).popoverController;        
    }    
    
    if ([[segue identifier] isEqualToString:@"AudioControls"]) {
        IRInterviewAudioPlayerViewController *newController = (IRInterviewAudioPlayerViewController *)destinationViewController;

        // Set the audio player
        newController.audioPlayer = self.audioPlayer;
    }
    
    if ([[segue identifier] isEqualToString:@"ViewQuestionResponse"]) {
        IRViewQuestionResponseiPhoneViewController *newController = (IRViewQuestionResponseiPhoneViewController *)destinationViewController;
        
        // Set the audio player
        newController.questionResponse = (IRQuestionResponse*)sender;
        newController.response = self.response;
    }
    
    if ([[segue identifier] isEqualToString:@"PlayAudio"]) {
        IRInterviewAudioPlayerViewController *newController = (IRInterviewAudioPlayerViewController *)destinationViewController;
        
        // Set the audio player
        newController.audioPlayer = self.audioPlayer;
        
        // Start the audio
        [newController.audioPlayer playInterview];
    }
}

- (IBAction)provideAppFeedback:(id)sender 
{
    // Show the UserVoice screen
    [IRUserVoiceHelper presentUserVoiceModalViewControllerForParent:self];
}
@end
