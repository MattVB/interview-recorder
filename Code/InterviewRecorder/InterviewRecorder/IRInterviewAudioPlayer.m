//
//  IRInterviewAudioPlayer.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 21/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IRInterviewAudioPlayer.h"
#import "IRResponse.h"
#import "IRQuestionResponse.h"
#import "IRAudioFileHelper.h"
#import "IRAudioResponseSegment.h"

#import <AVFoundation/AVFoundation.h>

@interface IRInterviewAudioPlayer ()
    @property AVAudioPlayer *player;
    @property IRResponse *response;
    @property NSArray *segments;

@end

@implementation IRInterviewAudioPlayer

    @synthesize player = _player;
    @synthesize response = _response;
    @synthesize segments = _segments;

-(id)initWithResponse:(IRResponse *)inputResponse
{
    self = [super init];
    if (self) {
        self.response = inputResponse;
        
        // If there is no audio file then don't create an audio player.
        if(!self.response.audioFileName)
        {
            return nil;
        }
        
        // Pull out the ordered segments - we need these so that we can skip forward/back
        self.segments = [self.response getOrderedAudioSegmentStartTimes];
        
        // Get the file name
        IRAudioFileHelper *audioFileHelper = [[IRAudioFileHelper alloc]init];
        NSString *audioFilePath = [audioFileHelper getFullPathForAudioFileName:self.response.audioFileName];
        
        // Create the URl
        NSURL *soundFileURL = [[NSURL alloc] initFileURLWithPath: audioFilePath];
        
        // Create the audio player
        NSError *error;
        self.player = [[AVAudioPlayer alloc]initWithContentsOfURL:soundFileURL error:&error];
        
        // Get ready to play
        [self.player prepareToPlay];
        
        return self;
    }
    
    return nil;
}

-(void)playInterview
{
    // Start playing from the start
    [self skipToTime:0];
}

-(void)playQuestionResponse:(IRQuestionResponse *)questionResponse
{
    if (questionResponse.audioSegments.count == 0)
    {
        // No audio segments
        return;
    }
    
    // Play the first audio segment
    IRAudioResponseSegment *segment = [questionResponse.audioSegments objectAtIndex:0];
    
    // Start playing from the relevant time
    [self skipToTime:segment.startTime];
}

-(void)pause
{
    // Pause
    [self.player pause];
}

-(void)resume
{
    // Restart the playback from where it left off
    [self.player play];
}


// Called when finished playing. Needs to release the resources we have used.
-(void)stop
{
    // Stop the audio player
    [self.player stop];
    
    // Release the audio player
    self.player = nil;
}

-(BOOL)playing
{
    return self.player.playing;
}

-(NSTimeInterval)duration
{
    return self.player.duration;
}

-(NSTimeInterval)currentTime
{
    return self.player.currentTime;
}

-(void)skipToTime:(NSTimeInterval)timeToSkipTo
{
    // Pause the player
    [self.player pause];
    
    // Move to the location
    self.player.currentTime = timeToSkipTo;
    
    // Resume playing
    [self.player play];
}

-(IRQuestionResponse*)currentlyPlayingQuestion
{
    NSTimeInterval currentAudioTime = self.player.currentTime;
    
    for (IRQuestionResponse *question in self.response.questions)
    {
        if ([question currentAudioTimeRelatesToQuestion:currentAudioTime])
        {
            return question;
        }
    }
    
    // Unable to find the question
    return nil;
}

-(void)nextAudioSegment
{
    // Skip through until we find the next segment
    for(NSNumber *segmentStartTime in self.segments)
    {
        if ([segmentStartTime doubleValue] > self.player.currentTime)
        {
            [self skipToTime:[segmentStartTime doubleValue]];
            return;
        }
    }
}

-(void)previousAudioSegment
{
    NSNumber *lastSegmentStart = nil;
    NSNumber *previousSegmentStart = nil;
    
    // Skip through until we find the next segment
    for(NSNumber *segmentStartTime in self.segments)
    {
        if ([segmentStartTime doubleValue] > self.player.currentTime)
        {
            [self skipToTime:[previousSegmentStart doubleValue]];
            return;
        }
        
        // Update the values
        previousSegmentStart = lastSegmentStart;
        lastSegmentStart = segmentStartTime;
    }
    
    // Last segment so go to start of previous segment
    [self skipToTime:[previousSegmentStart doubleValue]];
}

@end
