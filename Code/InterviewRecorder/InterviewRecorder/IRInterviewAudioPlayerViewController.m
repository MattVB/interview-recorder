//
//  IRInterviewAudioPlayerViewController.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 21/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IRInterviewAudioPlayerViewController.h"
#import "IRDeviceHelper.h"

@interface IRInterviewAudioPlayerViewController ()

@property NSTimer *timer;
@property UIImage *pauseImage;
@property UIImage *playImage;

@end

@implementation IRInterviewAudioPlayerViewController
@synthesize buttonNext;
@synthesize buttonPrevious;
@synthesize sliderCurrentPosition;
@synthesize labelCurrentlyPlaying;
@synthesize buttonPlayPause;
@synthesize audioPlayer = _audioPlayer;

@synthesize timer = _timer;
@synthesize pauseImage = _pauseImage;
@synthesize playImage = _playImage;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Create the images for the Play/Pause button - these are assigned to the button programatically
    self.pauseImage = [UIImage imageNamed:@"Pause.png"];
    self.playImage = [UIImage imageNamed:@"Play.png"];
}

- (void)viewDidUnload
{
    [self setLabelCurrentlyPlaying:nil];
    [self setButtonPlayPause:nil];
    [self setSliderCurrentPosition:nil];
    [self setButtonPrevious:nil];
    [self setButtonNext:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Set the max value on the slider
    self.sliderCurrentPosition.maximumValue = self.audioPlayer.duration;
    
    // Create the timer - this will refresh the display every 100ms.
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateDisplay:) userInfo:nil repeats:YES]; 
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    if (![IRDeviceHelper isIpad])
    {
        // Stop the audio player and release
        [self.audioPlayer stop];
        self.audioPlayer = nil;
    }
    
    if (self.timer)
    {
        // Invalidate the timer
        [self.timer invalidate];
        
        // Release the timer
        self.timer = nil;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (IBAction)playPause:(id)sender {
    if(self.audioPlayer.playing)
    {
        // Player is playing - pause
        [self.audioPlayer pause];
    }
    else 
    {
        // Resume playing
        [self.audioPlayer resume];
    }
    
}

-(void)updateDisplay:(NSTimer*)theTimer
{
    // Update the play/pause button
    if (self.audioPlayer.playing)
    {
        [self.buttonPlayPause setImage:self.pauseImage forState:UIControlStateNormal];
    }
    else 
    {
        [self.buttonPlayPause setImage:self.playImage forState:UIControlStateNormal];
    }
    
    // Update the current slider value
    self.sliderCurrentPosition.value = [self.audioPlayer currentTime];
    
    // Get the current question
    IRQuestionResponse *currentResponse = [self.audioPlayer currentlyPlayingQuestion];
    
    // Set the currently playing label based on the relevant question
    if (currentResponse)
    {
        labelCurrentlyPlaying.text = currentResponse.question;
    }
    else 
    {
        labelCurrentlyPlaying.text = @"";
    }
}



- (IBAction)currentPositionValueChanged:(id)sender {
    // Move the current position
    [self.audioPlayer skipToTime:self.sliderCurrentPosition.value];
}


- (IBAction)previous:(id)sender 
{
    [self.audioPlayer previousAudioSegment];
}
- (IBAction)next:(id)sender 
{
    [self.audioPlayer nextAudioSegment];
}
@end
