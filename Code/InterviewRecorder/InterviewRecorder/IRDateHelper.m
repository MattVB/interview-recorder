//
//  IRDateHelper.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 30/07/12.
//
//

#import "IRDateHelper.h"

@implementation IRDateHelper

+(NSString*)formatDate:(NSDate *)date
{
    // Get today's date
    NSDate *today = [self dateOnlyFromDate:[NSDate date]];
    
    // If today then return this with the time value
    if ([today isEqualToDate:[self dateOnlyFromDate:date]])
    {
        return [self appendTimePortionOfDate:date toString:@"Today "];
    }
    
    NSDate *yesterday = [NSDate dateWithTimeIntervalSinceNow: -(60.0*60.0*24.0)];
    
    // If yesterday then return this with the time value
    if ([yesterday isEqualToDate:[self dateOnlyFromDate:date]])
    {
        return [self appendTimePortionOfDate:date toString:@"Yesterday "];
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateStyle = NSDateFormatterShortStyle;
    dateFormatter.timeStyle = NSDateFormatterShortStyle;
    
    return [dateFormatter stringFromDate:date];
}

+(NSDate*)dateOnlyFromDate:(NSDate*) date
{
    unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    NSDateComponents* components = [calendar components:flags fromDate:date];
    
    return [calendar dateFromComponents:components];
}

+(NSString*)appendTimePortionOfDate:(NSDate*)date toString:(NSString*)prefix
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.timeStyle = NSDateFormatterShortStyle;
    
    return [prefix stringByAppendingString:[dateFormatter stringFromDate:date]];
}

@end
