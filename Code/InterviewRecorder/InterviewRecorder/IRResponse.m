//
//  IRResponse.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 14/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IRResponse.h"
#import "IRQuestionResponse.h"
#import "IRAudioResponseSegment.h"
#import "IRDateHelper.h"

@implementation IRResponse

    @synthesize respondent = _respondent;
    @synthesize dateResponded = _dateResponded;
    @synthesize interviewer = _interviewer;
    @synthesize overallComments = _overallComments;
    @synthesize questions = _questions; 
    @synthesize audioFileName = _audioFileName;
    @synthesize grouping = _grouping;
    @synthesize id = _id;

-(id)initResponse
{
    self = [super init];
    if (self) {
        // Create ID using guid.
        CFUUIDRef udid = CFUUIDCreate(NULL);
        _id = (__bridge NSString *) CFUUIDCreateString(NULL, udid);
        
        return self;
    }
    
    return nil;
}

    -(void)encodeWithCoder:(NSCoder *)aCoder
    {
        [aCoder encodeObject:self.respondent forKey:@"respondent"];
        [aCoder encodeObject:self.dateResponded forKey:@"dateResponded"];
        [aCoder encodeObject:self.interviewer forKey:@"interviewer"];
        [aCoder encodeObject:self.overallComments forKey:@"overallComments"];
        [aCoder encodeObject:self.questions forKey:@"questions"];
        [aCoder encodeObject:self.audioFileName forKey:@"audioFileName"];
        [aCoder encodeObject:self.grouping forKey:@"grouping"];
        [aCoder encodeObject:self.id forKey:@"id"];
    }

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self.respondent = [aDecoder decodeObjectForKey:@"respondent"];
    self.dateResponded = [aDecoder decodeObjectForKey:@"dateResponded"];
    self.interviewer = [aDecoder decodeObjectForKey:@"interviewer"];
    self.overallComments = [aDecoder decodeObjectForKey:@"overallComments"];
    self.questions = [aDecoder decodeObjectForKey:@"questions"];
    self.audioFileName = [aDecoder decodeObjectForKey:@"audioFileName"];
    self.id = [aDecoder decodeObjectForKey:@"id"];
    
    if ([aDecoder containsValueForKey:@"grouping"])
    {
        self.grouping = [aDecoder decodeObjectForKey:@"grouping"];
    }
    else
    {
        self.grouping = @"Unspecified";
    }
    
    
    return self;
}

-(NSString*)getDescription
{
    // Format the date
    NSString *stringFromDate = [IRDateHelper formatDate:self.dateResponded];
    
    // Create the final string and return
    return [NSString stringWithFormat:@"%@ - %@", self.respondent, stringFromDate];
}

-(NSMutableArray*)getQuestions
{
    NSMutableArray *result = [[NSMutableArray alloc]init];
    
    for(IRQuestionResponse *question in self.questions)
    {
        [result addObject:question.question];
    }
    
    return result;
}

-(BOOL)anyQuestionsHaveFlags
{
    for(IRQuestionResponse *question in self.questions)
    {
        if (question.flags.count > 0)
        {
            return YES;
        }
    }
    
    return NO;    
}

-(NSArray*)getOrderedAudioSegmentStartTimes
{
    NSMutableArray *result = [[NSMutableArray alloc]init];
    
    // Add all of the audio segments together
    for(IRQuestionResponse *question in self.questions)
    {
        for(IRAudioResponseSegment *responseSegment in question.audioSegments)
        {
            [result addObject:[NSNumber numberWithDouble:responseSegment.startTime]];
        }
    }
    
    // Sort the array
    [result sortedArrayUsingSelector:@selector(compare:)];
    
    return result;
}

-(NSTimeInterval)getAudioLength
{
    NSTimeInterval lastEndTime = 0;
    
    // Add all of the audio segments together
    for(IRQuestionResponse *question in self.questions)
    {
        for(IRAudioResponseSegment *responseSegment in question.audioSegments)
        {
            if(responseSegment.endTime > lastEndTime)
            {
                lastEndTime = responseSegment.endTime;
            }
        }
    }
    
    return lastEndTime;
}


-(NSString*)getResponseSummary
{
    NSTimeInterval audioLength = [self getAudioLength];
    
    if (audioLength == 0)
    {
        return [NSString stringWithFormat:@"%i questions - No audio - %@", self.questions.count, self.overallComments];
    }
    else {
        // Create the length value
        NSInteger minutes = audioLength / 60;
        // Add 1 to the minutes to take into account the random seconds 
        minutes++;
        
        // Create the audio length string
        NSString *audioString = [NSString stringWithFormat:@"%i mins audio", minutes];
        
        // Put the whole string together and return
        return [NSString stringWithFormat:@"%i questions - %@ - %@", self.questions.count, audioString, self.overallComments];
    }
}

-(IRQuestionResponse*)getQuestionResponseForQuestion:(NSString *)question
{
    for(IRQuestionResponse *questionResponse in self.questions)
    {
        if ([questionResponse.question isEqualToString:question])
        {
            return questionResponse;
        }
    }
    
    return nil;
}
    

@end
