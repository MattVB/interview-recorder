//
//  IRResponseFlags.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 18/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IRResponseFlags.h"

@implementation IRResponseFlags

+(NSString*)good{
    return @"Good";
}

+(NSString*)bad{
    return @"Bad";
}

+(NSString*)warning{
    return @"Warning";
}

+(NSString*)noFlag
{
    return @"No Flag";
}

+(NSString*)getImageNameForFlag:(NSString *)flag{
    
    if([flag isEqualToString:[self good]])
    {
        return @"HappyFace.png";
    }
    
    if([flag isEqualToString:[self bad]])
    {
        return @"SadFace.png";
    }
    
    if([flag isEqualToString:[self warning]])
    {
        return @"Warning.png";
    }
    
    if([flag isEqualToString:[self noFlag]])
    {
        return @"Empty.png";
    }
    
    return nil;
}

+(NSMutableArray*)getFlagsList
{
    NSMutableArray *result = [[NSMutableArray alloc]init];
    
    [result addObject:[IRResponseFlags noFlag]];
    [result addObject:[IRResponseFlags good]];
    [result addObject:[IRResponseFlags bad]];
    [result addObject:[IRResponseFlags warning]];
    
    return result;
}

+(NSInteger)getNumberOfFlags
{
    return [IRResponseFlags getFlagsList].count;
}


@end
