//
//  IRRecordResponseViewController.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 14/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IRResponse.h"
#import "IRNewQuestionViewController.h"
#import "IRCompleteInterviewViewController.h"
#import "IRSaveAsTemplateViewController.h"
#import "IRSelectGroupingViewController.h"
#import "IRSelectResponseFlagViewController.h"


@protocol IRRecordResponseViewControllerDelegate;


@interface IRRecordResponseViewController : UIViewController <IRNewQuestionViewControllerDelegate, UIAlertViewDelegate,  UIActionSheetDelegate, IRCompleteInterviewViewControllerDelegate, IRSaveAsTemplateViewControllerDelegate, IRSelectGroupingViewControllerDelegate, IRSelectResponseFlagViewControllerDelegate>


@property (weak, nonatomic) IBOutlet UIButton *buttonPrevious;
- (IBAction)previousQuetstion:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *labelQuestionNumber;
@property (weak, nonatomic) IBOutlet UILabel *labelQuestionText;
@property (weak, nonatomic) IBOutlet UITextView *textViewResponse;

- (IBAction)nextQuestion:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *buttonNextQuestion;
- (IBAction)deleteQuestion:(id)sender;
- (IBAction)buttonProgress:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *buttonProgress;
@property (weak, nonatomic) IBOutlet UITextField *textRespondentName;
- (IBAction)cancel:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *textGrouping;

- (IBAction)actionsButton:(id)sender;
@property (strong, nonatomic) IRResponse *response;
@property (nonatomic) NSInteger questionIndex;
@property (weak, nonatomic) IBOutlet UIButton *buttonGoodResponse;
- (IBAction)goodResponsePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *buttonBadResponse;
@property (weak, nonatomic) IBOutlet UIButton *buttonWarningResponse;
- (IBAction)badResponsePressed:(id)sender;
- (IBAction)warningResponsePressed:(id)sender;
- (IBAction)startPauseRecording:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *buttonStartPauseRecording;
- (IBAction)provideAppFeedback:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButtonRecordPauseAudio;
@property (weak, nonatomic) IBOutlet UIImageView *imageSelectedFlag;

@property (weak, nonatomic) id <IRRecordResponseViewControllerDelegate> delegate;
@end


@protocol IRRecordResponseViewControllerDelegate <NSObject>

-(void)recordResponseViewControllerComplete:(IRRecordResponseViewController*) controller response:(IRResponse*) response;

-(void)recordResponseViewControllerCancelled:(IRRecordResponseViewController*) controller;
    
@end
