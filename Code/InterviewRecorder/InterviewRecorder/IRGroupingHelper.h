//
//  IRGroupingHelper.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 29/07/12.
//
//

#import <Foundation/Foundation.h>

@interface IRGroupingHelper : NSObject

    -(id)initWithResponses:(NSMutableArray*)inputResponses;

    -(NSMutableArray*)getGroupings;

    -(NSMutableArray*)getResponsesWithGrouping:(NSString*)grouping;

    -(NSInteger)getResponseCountForGrouping:(NSString*)grouping;

@end
