//
//  IRAnalyseResponsesHelper.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 31/07/12.
//
//

#import "IRAnalyseResponsesHelper.h"
#import "IRResponse.h"
#import "IRQuestionResponse.h"

@interface IRAnalyseResponsesHelper ()

    @property NSMutableArray *responses;
    @property NSMutableArray *questions;
@end


@implementation IRAnalyseResponsesHelper

@synthesize responses = _responses;
@synthesize questions = _questions;

-(id)initWithResponses:(NSMutableArray*)inputResponses
{
    self = [super init];
    if (self) {
        self.responses = inputResponses;
        
        // Sort the responses based on date responded with most recent at the top.
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateResponded" ascending:FALSE];
        [self.responses sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        
        // Populate the questions
        [self populateQuestions];
        
        return self;
    }
    
    return nil;
}

-(NSMutableArray*)getQuestions
{
    return self.questions;
}

-(void)populateQuestions
{
    NSMutableArray *result = [[NSMutableArray alloc]init];
    
    for (IRResponse *response in self.responses)
    {
        for(IRQuestionResponse *question in response.questions)
        {
            if (![result containsObject:question.question])
            {
                [result addObject:question.question];
            }
        }
    }
    
    self.questions = result;
}

-(NSMutableArray*)getResponsesWithQuestion:(NSString*)question
{
    NSMutableArray *result = [[NSMutableArray alloc]init];
    
    for (IRResponse *response in self.responses)
    {
        if ([response getQuestionResponseForQuestion:question])
        {
            [result addObject:response];
        }
    }
    
    return result;
}

@end
