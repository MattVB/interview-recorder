//
//  IRConfirmSaveTemplateViewController.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 18/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IRConfirmSaveTemplateViewControllerDelegate;

@interface IRConfirmSaveTemplateViewController : UIViewController <UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *textTemplateName;
- (IBAction)templateNameValueChanged:(id)sender;
- (IBAction)save:(id)sender;

    @property (weak, nonatomic) id <IRConfirmSaveTemplateViewControllerDelegate> delegate;

@property (strong, nonatomic) NSString *suggestedTemplateName;
@end


@protocol IRConfirmSaveTemplateViewControllerDelegate <NSObject>

-(void)confirmSaveTemplateViewControllerComplete:(IRConfirmSaveTemplateViewController*)controller templateName:(NSString*) templateName;


@end