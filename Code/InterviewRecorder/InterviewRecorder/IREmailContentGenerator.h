//
//  IREmailContentGenerator.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 23/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IRResponse.h"

@interface IREmailContentGenerator : NSObject

-(NSString*)generateEmailSubjectForInterviewResponse:(IRResponse*)response;

-(NSString*)generateEmailContentForInterviewResponse:(IRResponse*)response;

@end
