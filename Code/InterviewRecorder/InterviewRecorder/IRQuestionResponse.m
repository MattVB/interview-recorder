//
//  IRQuestionResponse.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 14/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IRQuestionResponse.h"
#import "IRAudioResponseSegment.h"

@implementation IRQuestionResponse

    @synthesize question = _question;
    @synthesize response = _response;
    @synthesize flags = _flags;
    @synthesize audioSegments = _audioSegments;


-(id)initWithQuestion:(NSString *)inputQuestion
{
    self = [super init];
    if (self) {
        _question = inputQuestion;
        _flags = [[NSMutableArray alloc] init];
        _audioSegments = [[NSMutableArray alloc]init];
        
        return self;
    }
    
    return nil;
}

-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.question forKey:@"question"];
    [aCoder encodeObject:self.response forKey:@"response"];
    [aCoder encodeObject:self.flags forKey:@"flags"];
    [aCoder encodeObject:self.audioSegments forKey:@"audioSegments"];
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self.question = [aDecoder decodeObjectForKey:@"question"];
    self.response = [aDecoder decodeObjectForKey:@"response"];
    self.flags = [aDecoder decodeObjectForKey:@"flags"];
    self.audioSegments = [aDecoder decodeObjectForKey:@"audioSegments"];
    
    return self;
}

-(bool)currentAudioTimeRelatesToQuestion:(NSTimeInterval)currentAudioTime
{    
    // Check each segment and check if the current time falls within one of the segments related to this question
    for(IRAudioResponseSegment *responseSegment in self.audioSegments)
    {
        if (currentAudioTime >= responseSegment.startTime && currentAudioTime <= responseSegment.endTime)
        {
            return YES;
        }
    }
    
    return NO;
}

-(NSString*)getFormattedResponse
{
    if (!self.response || [self.response isEqualToString:@""])
    {
        return @"<No Response>";    
    }
    
    return self.response;
}


@end
