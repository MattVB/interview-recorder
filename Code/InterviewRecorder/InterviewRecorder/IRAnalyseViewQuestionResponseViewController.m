//
//  IRAnalyseViewQuestionResponseViewController.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 1/08/12.
//
//

#import "IRAnalyseViewQuestionResponseViewController.h"
#import "IRUiControlHelper.h"
#import "IRQuestionResponseAudioPlayer.h"

@interface IRAnalyseViewQuestionResponseViewController ()
@property IRQuestionResponseAudioPlayer *audioPlayer;
@end

@implementation IRAnalyseViewQuestionResponseViewController

@synthesize response = _response;
@synthesize questionResponse = _questionResponse;
@synthesize textViewQuestionResponse = _textViewQuestionResponse;
@synthesize buttonPlayAudio = _buttonPlayAudio;
@synthesize audioPlayer = _audioPlayer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Create the audio helper
    self.audioPlayer = [[IRQuestionResponseAudioPlayer alloc]initWithResponse:self.response andQuestion:self.questionResponse];
    
    
    [IRUiControlHelper formatTextView:self.textViewQuestionResponse];
    
    // Populate the UI
    self.textViewQuestionResponse.text = [self.questionResponse getFormattedResponse];
    
    // Hide the play audio button if there is no audio for this question
    if([self.audioPlayer hasAudioToPlay])
    {
        self.buttonPlayAudio.hidden = NO;
    }
    else
    {
        self.buttonPlayAudio.hidden = YES;
    }
}

- (void)viewDidUnload
{
    [self setTextViewQuestionResponse:nil];
    [self setButtonPlayAudio:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    // Stop the audio
    [self.audioPlayer stopAudio];
    
    // Release the audio player
    self.audioPlayer = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (IBAction)playAudio:(id)sender
{
    // Start playing the audio
    [self.audioPlayer playAudio];
}
@end
