//
//  IRResponse.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 14/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IRQuestionResponse.h"

@interface IRResponse : NSObject <NSCoding>

    @property (nonatomic, copy) NSString *respondent;
    @property (nonatomic, copy) NSDate *dateResponded;
    @property (nonatomic, copy) NSString *interviewer;
    @property (nonatomic, copy) NSString *overallComments;
    @property (nonatomic, copy) NSString *audioFileName;
    @property (nonatomic, copy) NSString *grouping;
    @property (nonatomic) NSMutableArray *questions;

    @property (nonatomic) NSString *id;

    -(id)initResponse;


    -(NSString*)getDescription;

    -(NSMutableArray*)getQuestions;

    -(BOOL)anyQuestionsHaveFlags;

    -(NSArray*)getOrderedAudioSegmentStartTimes;

    -(NSString*)getResponseSummary;

    -(IRQuestionResponse*)getQuestionResponseForQuestion:(NSString*)question;

@end
