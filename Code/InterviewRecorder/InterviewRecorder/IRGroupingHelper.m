//
//  IRGroupingHelper.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 29/07/12.
//
//

#import "IRGroupingHelper.h"
#import "IRResponse.h"

@interface IRGroupingHelper()
@property NSMutableArray *responses;

@end

@implementation IRGroupingHelper

@synthesize responses = _responses;

 -(id)initWithResponses:(NSMutableArray*)inputResponses
{
    self = [super init];
    if (self) {
        self.responses = inputResponses;
        
        // Sort the responses based on date responded with most recent at the top.
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateResponded" ascending:FALSE];
        [self.responses sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        
        return self;
    }
    
    return nil;
}

-(NSMutableArray*)getGroupings
{
    NSMutableArray *result = [[NSMutableArray alloc]init];
    
    // Get the distinct groupings
    for (IRResponse *response in self.responses)
    {
        if (![result containsObject:response.grouping])
        {
            [result addObject:response.grouping];
        }
    }
    
    return result;
}

-(NSMutableArray*)getResponsesWithGrouping:(NSString *)grouping
{
    NSMutableArray *result = [[NSMutableArray alloc]init];
    
    // Get the responses that matches the grouping
    for (IRResponse *response in self.responses)
    {
        if ([response.grouping isEqualToString:grouping])
        {
            [result addObject:response];
        }
    }
    
    return result;
}

-(NSInteger)getResponseCountForGrouping:(NSString *)grouping
{
    return [self getResponsesWithGrouping:grouping].count;
}

@end
