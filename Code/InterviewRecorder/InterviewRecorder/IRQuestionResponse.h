//
//  IRQuestionResponse.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 14/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IRQuestionResponse : NSObject <NSCoding>

@property (nonatomic, copy) NSString *question;
@property (nonatomic, copy) NSString *response;
@property (nonatomic) NSMutableArray *flags;
@property (nonatomic) NSMutableArray *audioSegments;


-(id)initWithQuestion:(NSString*)inputQuestion;

-(bool)currentAudioTimeRelatesToQuestion:(NSTimeInterval)currentAudioTime;

-(NSString*)getFormattedResponse;


@end
