//
//  IRViewResponseViewController.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 15/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IRResponse.h"

#import <AVFoundation/AVFoundation.h>
#import <MessageUI/MessageUI.h>

@interface IRViewResponseViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, AVAudioPlayerDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate>
    @property (weak, nonatomic) IBOutlet UILabel *labelGrouping;

    @property (strong, nonatomic) IRResponse *response;
    @property (weak, nonatomic) IBOutlet UITableView *tableView;
    @property (weak, nonatomic) IBOutlet UILabel *labelRespondentName;
    @property (weak, nonatomic) IBOutlet UILabel *labelDate;
    @property (weak, nonatomic) IBOutlet UILabel *labelInterviewerName;
    @property (weak, nonatomic) IBOutlet UILabel *labelOverallComments;
    @property (weak, nonatomic) IBOutlet UIButton *audioButton;
- (IBAction)provideAppFeedback:(id)sender;

@end
