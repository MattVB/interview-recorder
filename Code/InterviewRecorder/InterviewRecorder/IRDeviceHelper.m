//
//  IRDeviceHelper.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 15/08/12.
//
//

#import "IRDeviceHelper.h"

@implementation IRDeviceHelper

+(BOOL)isIpad
{
    NSString *device = [UIDevice currentDevice].model;
    
    return [device hasPrefix:@"iPad"];
}

+(void)dismissPopoverOrPopFromController:(UIViewController *)viewController withPopoverController:(UIPopoverController *)popoverController
{
    if ([IRDeviceHelper isIpad])
    {
        // Dismiss the popup
        [popoverController dismissPopoverAnimated:YES];
    }
    else
    {
        // Dismiss the navigation controller
        [viewController.navigationController popViewControllerAnimated:YES];
    }
}

+(BOOL)shouldAutoRotatePortaitOnlyForPhone:(UIInterfaceOrientation)interfaceOrientation
{
    if ([IRDeviceHelper isIpad])
    {
        return YES;
    }

    if (UIInterfaceOrientationIsPortrait(interfaceOrientation))
    {
        return YES;
    }
    
    return NO;
}

@end
