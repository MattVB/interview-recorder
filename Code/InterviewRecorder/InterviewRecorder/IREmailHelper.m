//
//  IREmailHelper.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 23/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IREmailHelper.h"
#import "IREmailContentGenerator.h"
#import "IRAudioFileHelper.h"

@implementation IREmailHelper

-(MFMailComposeViewController*)createMailComposeViewControllerForInterviewResponse:(IRResponse *)response  withAudio:(BOOL)withAudio
{
    // Create the mail controller and set the delegate
    MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
    
    // Create the email generator
    IREmailContentGenerator *emailContentGenerator = [[IREmailContentGenerator alloc]init];
    
    // Set the message subject
    [mailer setSubject:[emailContentGenerator generateEmailSubjectForInterviewResponse:response]];
    
    // Set the content
    [mailer setMessageBody:[emailContentGenerator generateEmailContentForInterviewResponse:response] isHTML:NO];
    
    // Add on the audio as an attachment
    if (response.audioFileName && withAudio)
    {
        // Get the full name for the file
        IRAudioFileHelper *audioFileHelper = [[IRAudioFileHelper alloc]init];
        NSString *fullAudioFileName = [audioFileHelper getFullPathForAudioFileName:response.audioFileName];
        
        // Load the audo data
        NSData *audioData = [NSData dataWithContentsOfFile:fullAudioFileName];
        
        // Add the attachment
        [mailer addAttachmentData:audioData mimeType:@"audio/x-caf" fileName:[NSString stringWithFormat:@"%@.caf", response.getDescription]];
    }
    
   
    
    return mailer;
}

@end
