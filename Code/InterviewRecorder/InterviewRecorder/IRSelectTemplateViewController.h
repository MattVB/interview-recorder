//
//  IRSelectTemplateViewController.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 15/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IRTemplate.h"

@protocol IRSelectTemplateViewControllerDelegate;

@interface IRSelectTemplateViewController : UITableViewController
    @property (weak, nonatomic) id <IRSelectTemplateViewControllerDelegate> delegate;
@end

@protocol IRSelectTemplateViewControllerDelegate <NSObject>
    - (void)selectTemplateViewControllerDidFinish:(IRSelectTemplateViewController*)controller template:(IRTemplate*)template;
@end
