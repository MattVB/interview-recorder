//
//  IRUserVoiceHelper.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 26/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IRUserVoiceHelper : NSObject

    + (void)presentUserVoiceModalViewControllerForParent:(UIViewController *)viewController;

@end
