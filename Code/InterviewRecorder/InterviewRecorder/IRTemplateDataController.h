//
//  IRTemplateDataController.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 15/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IRTemplate.h"

@interface IRTemplateDataController : NSObject

    -(NSMutableArray*)getTemplates;

    -(void)storeTemplate:(IRTemplate*)template;

    -(void)deleteTemplateWithName:(NSString*)name;

    -(BOOL)hasTemplateWithName:(NSString*)name;

@end
