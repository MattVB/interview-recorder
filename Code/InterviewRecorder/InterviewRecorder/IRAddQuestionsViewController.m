//
//  IRAddQuestionsViewController.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 29/07/12.
//
//

#import "IRAddQuestionsViewController.h"
#import "IRUiControlHelper.h"

@interface IRAddQuestionsViewController ()

@end

@implementation IRAddQuestionsViewController
@synthesize textViewQuestions;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Format the text view
    [IRUiControlHelper formatTextView:self.textViewQuestions];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Put the focus into the field
    [self.textViewQuestions becomeFirstResponder];
}

- (void)viewDidUnload
{
    [self setTextViewQuestions:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (IBAction)saveQuestions:(id)sender
{
    NSMutableArray *questions = [[NSMutableArray alloc]init];
    
    // Need to split the questions
    for(NSString *question in [textViewQuestions.text componentsSeparatedByString:@"\n"])
    {
        // Trim the value
        NSString *trimmedQuestion = [question stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        // Add the question if it is not empty
        if (trimmedQuestion.length > 0)
        {
            [questions addObject:trimmedQuestion];
        }
    }
    
    // Call the delegate
    [self.delegate addQuestionsViewControllerDidFinish:self questions:questions];
}
@end
