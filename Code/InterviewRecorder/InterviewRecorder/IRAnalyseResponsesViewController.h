//
//  IRAnalyseResponsesViewController.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 31/07/12.
//
//

#import <UIKit/UIKit.h>
#import "IRSelectGroupingViewController.h"

@interface IRAnalyseResponsesViewController : UITableViewController <IRSelectGroupingViewControllerDelegate>

@end
