//
//  IRMaintainTemplatesListViewController.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 18/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IRTemplate.h"
#import "IREditTemplateViewController.h"

@protocol IRMaintainTemplatesListViewControllerDelegate;

@interface IRMaintainTemplatesListViewController : UITableViewController <IREditTemplateViewControllerDelegate>

    @property (weak, nonatomic) id <IRMaintainTemplatesListViewControllerDelegate> delegate;
    - (IBAction)addTemplate:(id)sender;

@end


@protocol IRMaintainTemplatesListViewControllerDelegate <NSObject>

-(void)maintainTemplateListViewControllerStartEditing:(IRMaintainTemplatesListViewController*)controller templateToEdit:(IRTemplate*)templateToEdit;

-(void)maintainTemplateListViewControllerAddTemplate:(IRMaintainTemplatesListViewController*)controller;

@end