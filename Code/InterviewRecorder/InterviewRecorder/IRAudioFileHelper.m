//
//  IRAudioFileHelper.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 21/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IRAudioFileHelper.h"

@implementation IRAudioFileHelper

    -(NSString *)getNewAudioFileName
    {
        // Create the file name - this is unique based on datetime
        NSDate *date = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyMMddhhmm"];
        return [formatter stringFromDate:date];
    }

-(NSString*)getFullPathForAudioFileName:(NSString *)audioFileName
{
    // Get the home directory
    NSString *homeDirectory = NSHomeDirectory();
    
    // Generate the file name for the audio file - this will be saved to the "Documents" folder
   return [homeDirectory stringByAppendingString: [NSString stringWithFormat:@"/Documents/%@.m4a", audioFileName, nil]];
}

-(void)deleteAudioFileWithName:(NSString *)audioFileName
{
    // Get the full path
    NSString* fullPath = [self getFullPathForAudioFileName:audioFileName];
    
    // Get the file manager
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // If the file exists then delete it.
    if ([fileManager fileExistsAtPath:fullPath])
    {
        [fileManager removeItemAtPath:fullPath error:nil];
    }
}

-(AVAudioRecorder*)createAudioRecorderWithFilename:(NSString *)fileName
{
    // Create the new URL
    NSURL *soundFileURL = [[NSURL alloc] initFileURLWithPath: fileName];
    
    // Get the audio quality
    AVAudioQuality audioQuality = [self getAudioQuality];
    
    // Setup the record settings
    NSDictionary *recordSettings =
    [[NSDictionary alloc] initWithObjectsAndKeys:
     [NSNumber numberWithFloat: 16000], AVSampleRateKey,
     [NSNumber numberWithInt: kAudioFormatMPEG4AAC], AVFormatIDKey,
     [NSNumber numberWithInt: 1], AVNumberOfChannelsKey,
     [NSNumber numberWithInt: audioQuality],
     AVEncoderAudioQualityKey,
     nil];
    
    // Create the audio recorder
    return [[AVAudioRecorder alloc] initWithURL: soundFileURL
                                settings: recordSettings
                                   error: nil];

}

-(AVAudioQuality)getAudioQuality
{
    NSString *audioQuality = [[NSUserDefaults standardUserDefaults] stringForKey:@"AudioQuality"];
    
    if ([audioQuality isEqualToString:@"Minimum"])
    {
        return AVAudioQualityMin;
    }
    
    if ([audioQuality isEqualToString:@"Low"])
    {
        return AVAudioQualityLow;
    }
    
    if ([audioQuality isEqualToString:@"High"])
    {
        return AVAudioQualityHigh;
    }
    
    
    if ([audioQuality isEqualToString:@"Maximum"])
    {
        return AVAudioQualityMax;
    }
    
    // If in doubt then go with medium
    return AVAudioQualityMedium;
}

@end
