//
//  IRSelectResponseFlagViewController.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 18/08/12.
//
//

#import <UIKit/UIKit.h>

@protocol IRSelectResponseFlagViewControllerDelegate;

@interface IRSelectResponseFlagViewController : UITableViewController

    @property (weak, nonatomic) id <IRSelectResponseFlagViewControllerDelegate> delegate;

@end


@protocol IRSelectResponseFlagViewControllerDelegate <NSObject>
- (void)selectResponseFlagViewControllerDidFinish:(IRSelectResponseFlagViewController*)controller flag:(NSString *)flag;
@end