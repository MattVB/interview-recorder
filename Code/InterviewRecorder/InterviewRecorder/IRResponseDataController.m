//
//  IRResponseDataController.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 15/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IRResponseDataController.h"
#import "IRAudioFileHelper.h"

@implementation IRResponseDataController

static NSString *responseListKey = @"ResponsesList";

-(NSMutableArray*)getResponses
{
    // Get the value from the UserDefaults object
    NSUserDefaults *userSettings = [NSUserDefaults standardUserDefaults];
    NSData *data = [userSettings objectForKey:responseListKey];
    
    // If there is data then unarchive and return
    if (data)
    {
        NSArray *arr = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        NSMutableArray *result = [[NSMutableArray alloc]initWithArray:arr];
        
        // Check for if there are no IDs and if so then set IDs and store
        if (result.count > 0)
        {
            IRResponse *firstResponse = [result objectAtIndex:0];
            
            // If no ID set then set response IDs and save
            if (!firstResponse.id)
            {
                [self setResonseIdsAndSave:result];
            }
        }
        
        // Return the result
        return result;
    }
    
    // Create a new array and return
    return [[NSMutableArray alloc]init];
}

-(void)storeResponse:(IRResponse *)response
{
    // Check for empty respondent name
    if (!response.respondent || [response.respondent isEqualToString:@""])
    {
        response.respondent = @"Unknown";
    }
    
    // Check for empty grouping
    if (!response.grouping || [response.grouping isEqualToString:@""])
    {
        response.grouping = @"Unspecified";
    }
    
    // Get the list
    NSMutableArray *array = [self getResponses];
    
    // Add to the list
    [array addObject:response];
    
    // Store the updated list
    [self setResponses:array];
}


-(void)setResponses:(NSMutableArray*)responses
{
    // Archive the data
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:responses];
    
    // Store to the UserDefaults
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:responseListKey];
}

-(void)deleteResponse:(IRResponse *)response
{
    // Get the responses and the response to delete
    NSMutableArray *responses = [self getResponses];
    IRResponse *responseToDelete = [self getResponseWithId:response.id fromArray:responses];
    
    // If a response was found then delete the response
    if (responseToDelete)
    {
        if (responseToDelete.audioFileName)
        {
            // Delete the audio file associated if there is one
            IRAudioFileHelper *audioFileHelper = [[IRAudioFileHelper alloc]init];
            [audioFileHelper deleteAudioFileWithName:responseToDelete.audioFileName];
        }
        
        // Remove the response
        [responses removeObject:responseToDelete];
    }
    
    // Store the response back
    [self setResponses:responses];
}

-(IRResponse*)getResponseWithId:(NSString *)id
{
    // Get the responses
    NSMutableArray *responses = [self getResponses];
    
    // Return the relevant response
    return [self getResponseWithId:id fromArray:responses];
}

-(IRResponse*)getResponseWithId:(NSString*)id fromArray:(NSMutableArray*)responses
{
    for (IRResponse *response in responses)
    {
        if ([response.id isEqualToString:id])
        {
            return response;
        }
    }
    
    return nil;
}

-(void)deleteResponseAtIndex:(NSInteger)index
{
    // Get the responses and the response to delete
    NSMutableArray *responses = [self getResponses];
    IRResponse *response = [responses objectAtIndex:index];
    
    // If a response was found then delete the response
    if (response)
    {
        if (response.audioFileName)
        {
            // Delete the audio file associated if there is one
            IRAudioFileHelper *audioFileHelper = [[IRAudioFileHelper alloc]init];
            [audioFileHelper deleteAudioFileWithName:response.audioFileName];
        }
        
        // Remove the response
        [responses removeObject:response];
    }
    
    // Store the response back
    [self setResponses:responses];
}

-(NSMutableArray*)getResponsesForGrouping:(NSString *)grouping
{
    NSMutableArray *result = [[NSMutableArray alloc]init];
    
    // Get the responses that matches the grouping
    for (IRResponse *response in [self getResponses])
    {
        if ([response.grouping isEqualToString:grouping])
        {
            [result addObject:response];
        }
    }
    
    return result;
}

-(void)setResonseIdsAndSave:(NSMutableArray*) responses
{
    // Set the IDs to new Guids
    for(IRResponse *responseToUpdate in responses)
    {
        CFUUIDRef udid = CFUUIDCreate(NULL);
        responseToUpdate.id = (__bridge NSString *) CFUUIDCreateString(NULL, udid);
    }
    
    // Save the data
    [self setResponses:responses];
}

@end
