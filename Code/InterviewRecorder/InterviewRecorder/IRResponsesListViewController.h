//
//  IRResponsesListViewController.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 15/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IRSelectTemplateViewController.h"
#import "IRRecordResponseViewController.h"
#import "IRMaintainTemplatesListViewController.h"

@interface IRResponsesListViewController : UITableViewController <IRSelectTemplateViewControllerDelegate, IRRecordResponseViewControllerDelegate, IRMaintainTemplatesListViewControllerDelegate, UIActionSheetDelegate>
- (IBAction)provideAppFeedback:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonAnalyseReponses;
- (IBAction)showActionsList:(id)sender;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButtonItemActions;

@end
