//
//  IRSelectResponseFlagViewController.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 18/08/12.
//
//

#import "IRSelectResponseFlagViewController.h"
#import "IRResponseFlags.h"

@interface IRSelectResponseFlagViewController ()

@end

@implementation IRSelectResponseFlagViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [IRResponseFlags getNumberOfFlags];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"FlagCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Get the flag and set the text
    NSString *flag = [[IRResponseFlags getFlagsList] objectAtIndex:indexPath.row];
    cell.textLabel.text = flag;
    
    // Set the image
    NSString *flagImageName = [IRResponseFlags getImageNameForFlag:flag];
    cell.imageView.image = [UIImage imageNamed:flagImageName];  
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Get the flag that was selected
    NSString *flag = [[IRResponseFlags getFlagsList] objectAtIndex:indexPath.row];
    
    // Call the delegate
    [self.delegate selectResponseFlagViewControllerDidFinish:self flag:flag];
}

@end
