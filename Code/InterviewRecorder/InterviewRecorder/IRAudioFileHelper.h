//
//  IRAudioFileHelper.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 21/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

// Helper class for dealing with audio files
@interface IRAudioFileHelper : NSObject

    // Get the name of a new audio file. The name is based on a datetime stamp so that the name
    // is unlikely to clash with other files.
    -(NSString*)getNewAudioFileName; 


    // Takes the short name for an audio file and returns the full path of the file.
    -(NSString*)getFullPathForAudioFileName:(NSString*)audioFileName;


    // Deletes the audio file with the given name, if the file exists.
    -(void)deleteAudioFileWithName:(NSString*)audioFileName;

    -(AVAudioRecorder*)createAudioRecorderWithFilename:(NSString*)fileName;

@end
