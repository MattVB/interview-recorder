//
//  IRConfirmSaveTemplateViewController.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 18/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IRConfirmSaveTemplateViewController.h"
#import "IRTemplateDataController.h"


@interface IRConfirmSaveTemplateViewController ()

@end

@implementation IRConfirmSaveTemplateViewController
@synthesize textTemplateName;
@synthesize suggestedTemplateName = _suggestedTemplateName;
@synthesize delegate = _delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setTextTemplateName:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Set the suggested template name
    textTemplateName.text = self.suggestedTemplateName;
    
    // Give the focus to the text field
    [textTemplateName becomeFirstResponder];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (IBAction)templateNameValueChanged:(id)sender 
{
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)save:(id)sender {
    if ([textTemplateName.text isEqualToString:@""])
    {
        // No template name entered - show alert with this information.
         UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"You must enter a template name." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alertView show];
        return;
    }
    
    // Template already exists with name. Confirm overwrite
    IRTemplateDataController *templateDataController = [[IRTemplateDataController alloc]init];
    if ([templateDataController hasTemplateWithName:textTemplateName.text])
    {
        // No template name entered - show alert with this information.
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"A template already exists with this name. Do you want to overwrite this template?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: @"Overwrite", nil];
        [alertView show];
        return;
    }

    // Call the delegate
    [self.delegate confirmSaveTemplateViewControllerComplete:self templateName:textTemplateName.text];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        // Confirmed - Call the delegate
        [self.delegate confirmSaveTemplateViewControllerComplete:self templateName:textTemplateName.text];
    }
}
@end
