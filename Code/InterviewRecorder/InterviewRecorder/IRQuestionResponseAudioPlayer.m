//
//  IRQuestionResponseAudioPlayer.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 1/08/12.
//
//

#import "IRQuestionResponseAudioPlayer.h"
#import "IRAudioFileHelper.h"
#import "IRAudioResponseSegment.h"
#import <AVFoundation/AVFoundation.h>


@interface IRQuestionResponseAudioPlayer ()
    @property AVAudioPlayer *player;
    @property NSTimer *timer;

@end

@implementation IRQuestionResponseAudioPlayer

@synthesize response = _response;
@synthesize questionResponse = _questionResponse;
@synthesize player = _player;

-(id)initWithResponse:(IRResponse *)inputResponse andQuestion:(IRQuestionResponse *)inputQuestionResponse
{
    self = [super init];
    if (self) {
        self.response = inputResponse;
        self.questionResponse = inputQuestionResponse;
        
        // If there is no audio file then don't create an audio player
        if(!self.response.audioFileName || self.questionResponse.audioSegments.count == 0)
        {
            return self;
        }
        
        // Get the file name
        IRAudioFileHelper *audioFileHelper = [[IRAudioFileHelper alloc]init];
        NSString *audioFilePath = [audioFileHelper getFullPathForAudioFileName:self.response.audioFileName];
        
        // Create the URl
        NSURL *soundFileURL = [[NSURL alloc] initFileURLWithPath: audioFilePath];
        
        // Create the audio player
        NSError *error;
        self.player = [[AVAudioPlayer alloc]initWithContentsOfURL:soundFileURL error:&error];
        
        // Get ready to play
        [self.player prepareToPlay];
        
        return self;
    }
    
    return nil;
}

-(BOOL)hasAudioToPlay{
    if (self.player)
    {
        return true;
    }
    
    return false;
}

-(void)playAudio
{
    // Determine where to play from
    IRAudioResponseSegment *audioSegment = [self.questionResponse.audioSegments objectAtIndex:0];
    self.player.currentTime = audioSegment.startTime;
    
    // Start playing
    [self.player play];
    
    // Invalidate the timer if it is already running
    [self.timer invalidate];
    
    // Setup timer to stop at the end
    NSTimeInterval length = audioSegment.endTime - audioSegment.startTime;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:length target:self selector:@selector(stopAudio) userInfo:nil repeats:NO];
}

-(void)stopAudio
{
    [self.player stop];
    
    // Invalidate the timer
    [self.timer invalidate];
}

@end
