//
//  IRSelectGroupingViewController.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 29/07/12.
//
//

#import "IRSelectGroupingViewController.h"
#import "IRResponseDataController.h"
#import "IRGroupingHelper.h"

@interface IRSelectGroupingViewController ()

@end

@implementation IRSelectGroupingViewController

IRGroupingHelper *groupingHelper;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Set the title
    self.navigationItem.title = @"Select Grouping";
    
    // Load responses
    IRResponseDataController *dataController = [[IRResponseDataController alloc]init];
    NSMutableArray *responses = [dataController getResponses];
    
    // Create grouping helper
    groupingHelper = [[IRGroupingHelper alloc]initWithResponses:responses];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [groupingHelper getGroupings].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"GroupingCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.textLabel.text = [[groupingHelper getGroupings] objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Determine the grouping selected
    NSString *grouping = [[groupingHelper getGroupings] objectAtIndex:indexPath.row];
    
    // Call the delegate
    [self.delegate selectGroupingViewControllerDidFinish:self grouping:grouping];
}

@end
