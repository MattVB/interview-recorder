//
//  IRTemplate.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 15/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IRTemplate : NSObject <NSCoding>

    @property (nonatomic, copy) NSString *templateName;
    @property (nonatomic, strong) NSMutableArray *questions;

    -(id)initWitTemplateName:(NSString*)inputTemplateName;

    -(void)addQuestion:(NSString*)question;

@end
