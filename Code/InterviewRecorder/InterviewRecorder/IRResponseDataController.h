//
//  IRResponseDataController.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 15/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IRResponse.h"

@interface IRResponseDataController : NSObject

    -(NSMutableArray*)getResponses;

    -(void)storeResponse:(IRResponse*)response;

    -(IRResponse*)getResponseWithId:(NSString*)id;

    -(void)deleteResponse:(IRResponse*)response;

    -(NSMutableArray*)getResponsesForGrouping:(NSString*)grouping;

@end
