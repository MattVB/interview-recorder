//
//  IRAnalyseResponsesHelper.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 31/07/12.
//
//

#import <Foundation/Foundation.h>

@interface IRAnalyseResponsesHelper : NSObject

    -(id)initWithResponses:(NSMutableArray*)inputResponses;

    -(NSMutableArray*)getQuestions;

-(NSMutableArray*)getResponsesWithQuestion:(NSString*)question;

@end
