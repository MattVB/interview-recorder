//
//  IREditTemplateViewController.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 18/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IREditTemplateViewController.h"
#import "IRTemplateDataController.h"
#import "IRConfirmSaveTemplateViewController.h"
#import "IRDeviceHelper.h"


@interface IREditTemplateViewController ()
 @property UIPopoverController *popoverController;
@end

@implementation IREditTemplateViewController

@synthesize popoverController = __popoverController;
@synthesize template = _template;
@synthesize delegate = _delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Set the navigation title. If existing template then use template now, otherwise just New Template
    if ([self.template.templateName isEqualToString:@""])
    {
        self.navigationItem.title = @"New Template";
    }
    else 
    {
        self.navigationItem.title = [NSString stringWithFormat:@"Template: %@", self.template.templateName];
    }
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.template.questions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TemplateQuestionCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Get the question
    NSString *questionText = [self.template.questions objectAtIndex:indexPath.row];
    
    // Configure the cell...
    cell.textLabel.text = questionText;
    
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.template.questions removeObjectAtIndex:indexPath.row];
        
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}



// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    // Get the question
    NSString *questionToMove = [self.template.questions objectAtIndex:fromIndexPath.row];
    
    // Remove the question
    [self.template.questions removeObjectAtIndex:fromIndexPath.row];
    
    // Put the question back int
    [self.template.questions insertObject:questionToMove atIndex:toIndexPath.row];
}



// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}


#pragma mark - Table view delegate

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the destination view controller
    id destinationViewController = [segue destinationViewController];
    
    // Check for if this is a popover and if so - hold onto the popover controller so that we can dismiss later
    if ([segue isKindOfClass:[UIStoryboardPopoverSegue class]])
    {
        self.popoverController = ((UIStoryboardPopoverSegue*)segue).popoverController;
    }
    
    if ([[segue identifier] isEqualToString:@"AddQuestions"]) {
        IRAddQuestionsViewController *addQuestionsViewController = (IRAddQuestionsViewController *)destinationViewController;
        addQuestionsViewController.delegate = self;
    }
    
    if ([[segue identifier] isEqualToString:@"SaveTemplate"]) {
        IRConfirmSaveTemplateViewController *newViewController = (IRConfirmSaveTemplateViewController *)destinationViewController;
        newViewController.delegate = self;
        
        // Set the suggested template name
        newViewController.suggestedTemplateName = self.template.templateName;
    }
}

-(void)addQuestionsViewControllerDidFinish:(IRAddQuestionsViewController *)controller questions:(NSMutableArray *)questions
{
    // Add the questions to the list
    for(NSString *question in questions)
    {
        [self.template.questions addObject:question];
    }
    
    
    // Refresh the table view
    [self.tableView reloadData];
    
    // Close the Add questions view
    [IRDeviceHelper dismissPopoverOrPopFromController:controller withPopoverController:self.popoverController];
}



- (IBAction)cancel:(id)sender {
    // Dismiss the popup
    [self dismissModalViewControllerAnimated:YES];
}

-(void)confirmSaveTemplateViewControllerComplete:(IRConfirmSaveTemplateViewController *)controller templateName:(NSString *)templateName
{
    // Dismiss the view
    [IRDeviceHelper dismissPopoverOrPopFromController:controller withPopoverController:self.popoverController];
    
    // Update the template name
    self.template.templateName = templateName;
    
    // Do the save 
    IRTemplateDataController *templateDataController = [[IRTemplateDataController alloc]init];
    [templateDataController storeTemplate:self.template];
    
    // Call the delegate
    [self.delegate editTemplateControllerDone:self];
    
    // Close the template editing view
    [self dismissModalViewControllerAnimated:YES];
}

@end
