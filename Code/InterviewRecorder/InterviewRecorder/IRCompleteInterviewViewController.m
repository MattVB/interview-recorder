//
//  IRCompleteInterviewViewController.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 16/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IRCompleteInterviewViewController.h"
#import "IRDeviceHelper.h"

@interface IRCompleteInterviewViewController ()

@end

@implementation IRCompleteInterviewViewController
@synthesize textRespondentName;
@synthesize textViewOverallComments;

@synthesize textGrouping;
@synthesize delegate = _delegate;
@synthesize response = _response;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setTextRespondentName:nil];
    [self setTextViewOverallComments:nil];
    [self setTextGrouping:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Set the initial values
    textRespondentName.text = self.response.respondent;
    textViewOverallComments.text = self.response.overallComments;
    textGrouping.text = self.response.grouping;
    
    
    // If no respondent is set then push text focus to that field otherwise push text focus to the
    // overall comments field.
    if ([self.response.respondent isEqualToString:@""])
    {
        [textRespondentName becomeFirstResponder];
    }
    else 
    {
        [textViewOverallComments becomeFirstResponder];
    }

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Should be portrait only on phone
    return [IRDeviceHelper shouldAutoRotatePortaitOnlyForPhone:interfaceOrientation];
}

- (IBAction)complete:(id)sender 
{
    // Update the response
    self.response.respondent = textRespondentName.text;
    self.response.overallComments = textViewOverallComments.text;
    
    // Only update the grouping if it was visible on the screen
    if (textGrouping)
    {
        self.response.grouping = textGrouping.text;
    }
    
    // Call the delegate
    [self.delegate completeInterviewViewControllerDone:self response:self.response];
}
@end
