//
//  IRNewQuestionViewController.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 14/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IRNewQuestionViewControllerDelegate;

@interface IRNewQuestionViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *textQuestion;
- (IBAction)save:(id)sender;
@property (weak, nonatomic) id <IRNewQuestionViewControllerDelegate> delegate;

@end


@protocol IRNewQuestionViewControllerDelegate <NSObject>
- (void)newQuestionViewControllerDidFinish:(IRNewQuestionViewController*)controller question:(NSString *)question;
@end
