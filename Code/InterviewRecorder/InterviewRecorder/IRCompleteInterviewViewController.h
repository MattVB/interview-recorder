//
//  IRCompleteInterviewViewController.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 16/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IRResponse.h"

@protocol IRCompleteInterviewViewControllerDelegate;

@interface IRCompleteInterviewViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *textRespondentName;
@property (weak, nonatomic) IBOutlet UITextView *textViewOverallComments;
- (IBAction)complete:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *textGrouping;

@property (weak, nonatomic) id <IRCompleteInterviewViewControllerDelegate> delegate;
@property (strong, nonatomic) IRResponse *response;

@end

@protocol IRCompleteInterviewViewControllerDelegate <NSObject>

-(void)completeInterviewViewControllerDone:(IRCompleteInterviewViewController*)controller response:(IRResponse*)response;

@end