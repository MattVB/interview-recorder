//
//  IRSelectTemplateViewController.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 15/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IRSelectTemplateViewController.h"
#import "IRTemplate.h"
#import "IRTemplateDataController.h"
#import "IRResponse.h"
#import "IRResponseDataController.h"

@interface IRSelectTemplateViewController ()

@end

@implementation IRSelectTemplateViewController

@synthesize delegate = _delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Select Template";
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Templates";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Get the number of templates
    IRTemplateDataController *templateDataController = [[IRTemplateDataController alloc]init];
    return [templateDataController getTemplates].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TemplateCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (indexPath.section == 0)
    {
        IRTemplateDataController *templateDataController = [[IRTemplateDataController alloc]init];
        IRTemplate * template = [[templateDataController getTemplates]objectAtIndex:indexPath.row];
        
        cell.textLabel.text = template.templateName;
        return cell;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Get the template
    IRTemplateDataController *templateDataController = [[IRTemplateDataController alloc]init];
    IRTemplate *template = [[templateDataController getTemplates]objectAtIndex:indexPath.row];
        
    // Call the delegate
    [self.delegate selectTemplateViewControllerDidFinish:self template:template];
}

@end
