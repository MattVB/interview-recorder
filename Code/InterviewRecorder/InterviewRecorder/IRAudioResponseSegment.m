//
//  IRAudioResponseSegment.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 21/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IRAudioResponseSegment.h"

@implementation IRAudioResponseSegment

    @synthesize startTime = _startTime;
    @synthesize endTime = _endTime;

-(id)initWithStartTime:(NSTimeInterval)inputStartTime endTime:(NSTimeInterval)inputEndTime
{
    self = [super init];
    if (self) {
        _startTime = inputStartTime;
        _endTime = inputEndTime;
        
        return self;
    }
    
    return nil;
}

-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeDouble:self.startTime forKey:@"startTime"];
    [aCoder encodeDouble:self.endTime forKey:@"endTime"];
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self.startTime = [aDecoder decodeDoubleForKey:@"startTime"];
    self.endTime = [aDecoder decodeDoubleForKey:@"endTime"];
    
    return self;
}

@end
