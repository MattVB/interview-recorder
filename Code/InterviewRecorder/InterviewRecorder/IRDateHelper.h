//
//  IRDateHelper.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 30/07/12.
//
//

#import <Foundation/Foundation.h>

@interface IRDateHelper : NSObject

+(NSString*)formatDate:(NSDate*)date;

@end
