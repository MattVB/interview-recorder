//
//  IRAnalyseViewQuestionResponseiPhoneViewController.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 18/08/12.
//
//

#import "IRViewQuestionResponseiPhoneViewController.h"
#import "IRQuestionResponseAudioPlayer.h"
#import "IRUiControlHelper.h"

@interface IRViewQuestionResponseiPhoneViewController ()
@property IRQuestionResponseAudioPlayer *audioPlayer;
@end

@implementation IRViewQuestionResponseiPhoneViewController
@synthesize labelRespondent;
@synthesize labelQuestionText;
@synthesize textViewResponse;
@synthesize buttonPlayAudio;
@synthesize audioPlayer = _audioPlayer;
@synthesize response = _response;
@synthesize questionResponse = _questionResponse;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    // Create the audio helper
    self.audioPlayer = [[IRQuestionResponseAudioPlayer alloc]initWithResponse:self.response andQuestion:self.questionResponse];
    
    
    [IRUiControlHelper formatTextView:self.textViewResponse];
    
    // Populate the UI
    self.textViewResponse.text = [self.questionResponse getFormattedResponse];
    self.labelQuestionText.text = self.questionResponse.question;
    self.labelRespondent.text = self.response.respondent;
    
    // Hide the play audio button if there is no audio for this question
    if([self.audioPlayer hasAudioToPlay])
    {
        self.buttonPlayAudio.hidden = NO;
    }
    else
    {
        self.buttonPlayAudio.hidden = YES;
    }
}

- (void)viewDidUnload
{
    [self setLabelRespondent:nil];
    [self setLabelQuestionText:nil];
    [self setTextViewResponse:nil];
    [self setButtonPlayAudio:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    // Stop the audio
    [self.audioPlayer stopAudio];
    
    // Release the audio player
    self.audioPlayer = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)playAudio:(id)sender {
    [self.audioPlayer playAudio];
}
@end
