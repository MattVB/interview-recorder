//
//  IREmailHelper.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 23/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IRResponse.h"
#import <MessageUI/MessageUI.h>

@interface IREmailHelper : NSObject

-(MFMailComposeViewController*)createMailComposeViewControllerForInterviewResponse:(IRResponse*)response withAudio:(BOOL)withAudio;

@end
