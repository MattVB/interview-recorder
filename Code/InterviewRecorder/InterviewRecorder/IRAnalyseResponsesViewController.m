//
//  IRAnalyseResponsesViewController.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 31/07/12.
//
//

#import "IRAnalyseResponsesViewController.h"
#import "IRAnalyseResponsesHelper.h"
#import "IRResponse.h"
#import "IRResponseDataController.h"
#import "IRResponseFlags.h"
#import "IRSelectGroupingViewController.h"
#import "IRAnalyseViewQuestionResponseViewController.h"
#import "IRDeviceHelper.h"
#import "IRViewQuestionResponseiPhoneViewController.h"

@interface IRAnalyseResponsesViewController ()
 @property IRAnalyseResponsesHelper *analyseResponseHelper;
 @property UIPopoverController *popoverController;
 @property NSString *groupingForFilter;
@end

@implementation IRAnalyseResponsesViewController

@synthesize analyseResponseHelper = _analyseResponseHelper;
@synthesize popoverController = __popoverController;
@synthesize groupingForFilter = _groupingForFilter;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Load the responses and create the analyse response helper
    IRResponseDataController *dataController = [[IRResponseDataController alloc]init];
    NSMutableArray *responsees = [dataController getResponses];
    self.analyseResponseHelper = [[IRAnalyseResponsesHelper alloc]initWithResponses:responsees];
    
    // Update the navigation item
    self.navigationItem.title = @"Analyse Responses";
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.analyseResponseHelper getQuestions].count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Get the question
    NSString *question = [[self.analyseResponseHelper getQuestions]objectAtIndex:section];
    
    // Return the number of responses
    return [self.analyseResponseHelper getResponsesWithQuestion:question].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AnalyseResponseCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Get the question
    NSString *question = [[self.analyseResponseHelper getQuestions]objectAtIndex:indexPath.section];
    
    // Get the response
    IRResponse *response = [[self.analyseResponseHelper getResponsesWithQuestion:question]objectAtIndex:indexPath.row];
    
    // Get the question response
    IRQuestionResponse *questionResponse = [response getQuestionResponseForQuestion:question];
    
    // Populate the cell
    cell.textLabel.text = [response getDescription];
    cell.detailTextLabel.text = [questionResponse getFormattedResponse];
    
    
        // Get the flag and display the appropriate image
        if (questionResponse.flags.count > 0)
        {
            NSString *firstFlag = [questionResponse.flags objectAtIndex:0];
            NSString *flagImageName = [IRResponseFlags getImageNameForFlag:firstFlag];
            cell.imageView.image = [UIImage imageNamed:flagImageName];
        }
        else
        {
            // No flags - display an empty image so that everything has the correct alignment
            cell.imageView.image = [UIImage imageNamed:@"Empty.png"];
        }
    
    return cell;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[self.analyseResponseHelper getQuestions]objectAtIndex:section];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([IRDeviceHelper isIpad])
    {
    // Create the new view controller
    IRAnalyseViewQuestionResponseViewController *questionResponseViewController = [[IRAnalyseViewQuestionResponseViewController alloc]init];
    
    // Get the question
    NSString *question = [[self.analyseResponseHelper getQuestions]objectAtIndex:indexPath.section];
    // Get the response
    IRResponse *response = [[self.analyseResponseHelper getResponsesWithQuestion:question]objectAtIndex:indexPath.row];
    // Get the question response
    IRQuestionResponse *questionResponse = [response getQuestionResponseForQuestion:question];
    
    // Set the properties
    questionResponseViewController.response = response;
    questionResponseViewController.questionResponse = questionResponse;

    // Create popover and display
    self.popoverController = [[UIPopoverController alloc]
                                              initWithContentViewController:questionResponseViewController];
    self.popoverController.popoverContentSize = CGSizeMake(520, 120);
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [self.popoverController presentPopoverFromRect:cell.bounds inView:cell.contentView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else
    {
        // Device is an iPhone - just push to the segue
        [self performSegueWithIdentifier:@"ViewQuestionResponse" sender:indexPath];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the destination view controller
    id destinationViewController = [segue destinationViewController];
    
    // Check for if this is a popover and if so - hold onto the popover controller so that we can dismiss later
    if ([segue isKindOfClass:[UIStoryboardPopoverSegue class]])
    {
        self.popoverController = ((UIStoryboardPopoverSegue*)segue).popoverController;
    }
    
    if ([[segue identifier] isEqualToString:@"SelectGrouping"]) {
        IRSelectGroupingViewController *newViewController = (IRSelectGroupingViewController *)destinationViewController;
        
        // Set the delegate and the response
        newViewController.delegate = self;
    }
    
    if ([[segue identifier] isEqualToString:@"ViewQuestionResponse"])
    {
        NSIndexPath *indexPath = (NSIndexPath*)sender;
        
        // Get the question
        NSString *question = [[self.analyseResponseHelper getQuestions]objectAtIndex:indexPath.section];
        // Get the response
        IRResponse *response = [[self.analyseResponseHelper getResponsesWithQuestion:question]objectAtIndex:indexPath.row];
        // Get the question response
        IRQuestionResponse *questionResponse = [response getQuestionResponseForQuestion:question];
        
        // Set the properties on the new view controller
        IRViewQuestionResponseiPhoneViewController *questionResponseViewController = (IRViewQuestionResponseiPhoneViewController*)destinationViewController;
        questionResponseViewController.response = response;
        questionResponseViewController.questionResponse = questionResponse;
    }
}

-(void)selectGroupingViewControllerDidFinish:(IRSelectGroupingViewController *)controller grouping:(NSString *)grouping
{
    // Set the grouping
    self.groupingForFilter = grouping;
    
    // Load the responses and create the analyse response helper
    IRResponseDataController *dataController = [[IRResponseDataController alloc]init];
    NSMutableArray *responsees = [dataController getResponsesForGrouping:grouping];
    self.analyseResponseHelper = [[IRAnalyseResponsesHelper alloc]initWithResponses:responsees];
    
    // Update the navigation item
    self.navigationItem.title = [@"Analyse Responses: " stringByAppendingString:grouping];
    
    // Refresh data
    [self.tableView reloadData];
    
    // Dismiss the view
    [IRDeviceHelper dismissPopoverOrPopFromController:controller withPopoverController:self.popoverController];
}

@end
