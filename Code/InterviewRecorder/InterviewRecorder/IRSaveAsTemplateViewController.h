//
//  IRSaveAsTemplateViewController.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 17/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IRSaveAsTemplateViewControllerDelegate;

@interface IRSaveAsTemplateViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *textTemplateName;
- (IBAction)save:(id)sender;

@property (weak, nonatomic) id <IRSaveAsTemplateViewControllerDelegate> delegate;

@end


@protocol IRSaveAsTemplateViewControllerDelegate <NSObject>

-(void)saveAsTemplateControllerDone:(IRSaveAsTemplateViewController*)controller templateName:(NSString*)templateName;

@end
