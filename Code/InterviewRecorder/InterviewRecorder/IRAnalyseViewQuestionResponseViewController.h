//
//  IRAnalyseViewQuestionResponseViewController.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 1/08/12.
//
//

#import <UIKit/UIKit.h>
#import "IRResponse.h"
#import "IRQuestionResponse.h"

@interface IRAnalyseViewQuestionResponseViewController : UIViewController

@property (weak, nonatomic) IRResponse *response;
@property (weak, nonatomic) IRQuestionResponse *questionResponse;
@property (weak, nonatomic) IBOutlet UITextView *textViewQuestionResponse;
@property (weak, nonatomic) IBOutlet UIButton *buttonPlayAudio;
- (IBAction)playAudio:(id)sender;

@end
