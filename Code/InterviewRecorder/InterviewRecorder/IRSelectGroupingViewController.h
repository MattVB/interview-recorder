//
//  IRSelectGroupingViewController.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 29/07/12.
//
//

#import <UIKit/UIKit.h>

@protocol IRSelectGroupingViewControllerDelegate;

@interface IRSelectGroupingViewController : UITableViewController

    @property (weak, nonatomic) id <IRSelectGroupingViewControllerDelegate> delegate;

@end

@protocol IRSelectGroupingViewControllerDelegate <NSObject>
- (void)selectGroupingViewControllerDidFinish:(IRSelectGroupingViewController*)controller grouping:(NSString *)grouping;
@end
