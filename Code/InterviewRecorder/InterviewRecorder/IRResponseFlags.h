//
//  IRResponseFlags.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 18/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IRResponseFlags : NSObject
    +(NSString*)good;
    +(NSString*)bad;
    +(NSString*)warning;
    +(NSString*)noFlag;

    +(NSString*)getImageNameForFlag:(NSString*) flag;

+(NSInteger)getNumberOfFlags;

+(NSMutableArray*)getFlagsList;
@end
