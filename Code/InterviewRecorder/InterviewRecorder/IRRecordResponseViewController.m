//
//  IRRecordResponseViewController.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 14/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IRRecordResponseViewController.h"
#import "IRQuestionResponse.h"
#import "IRNewQuestionViewController.h"
#import "IRResponseDataController.h"
#import "IRCompleteInterviewViewController.h"
#import "IRSaveAsTemplateViewController.h"
#import "IRTemplate.h"
#import "IRTemplateDataController.h"
#import "IRResponseFlags.h"
#import "IRAudioFileHelper.h"
#import "IRAudioResponseSegment.h"
#import "IRUiControlHelper.h"
#import "IRDeviceHelper.h"
#import "IRUserVoiceHelper.h"



#import <AVFoundation/AVFoundation.h>



@interface IRRecordResponseViewController ()

 @property UIPopoverController *popoverController;
@property AVAudioRecorder *audioRecorder;
@property BOOL recorded;
@property NSTimeInterval currentQuestionStartTime;

@end

@implementation IRRecordResponseViewController
@synthesize textGrouping;
@synthesize buttonProgress;
@synthesize textRespondentName;
@synthesize labelQuestionText;
@synthesize textViewResponse;
@synthesize buttonNextQuestion;
@synthesize labelQuestionNumber;
@synthesize buttonPrevious;

@synthesize response = _response;
@synthesize questionIndex = _questionIndex;
@synthesize buttonGoodResponse;
@synthesize buttonBadResponse;
@synthesize buttonWarningResponse;

@synthesize buttonStartPauseRecording;
@synthesize barButtonRecordPauseAudio;
@synthesize imageSelectedFlag;
@synthesize delegate = _delegate;

@synthesize popoverController = __popoverController;
@synthesize audioRecorder = _audioRecorder;
@synthesize recorded = _recorded;
@synthesize currentQuestionStartTime = _currentQuestionStartTime;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Setup for sound recording
    [self setupAudioRecorder];
    
    // Set the respondent name
    self.textRespondentName.text = self.response.respondent;
    
    // Set the grouping
    self.textGrouping.text = self.response.grouping;
    
    // Configure the text response.
    [IRUiControlHelper formatTextView:self.textViewResponse];
    
    // Refresh the question display
    [self refreshQuestionDisplay];
}

- (void)viewDidUnload
{
    [self setButtonPrevious:nil];
    [self setLabelQuestionNumber:nil];
    [self setLabelQuestionText:nil];
    [self setTextViewResponse:nil];
    [self setButtonNextQuestion:nil];
    [self setButtonProgress:nil];
    [self setTextRespondentName:nil];
    [self setButtonGoodResponse:nil];
    [self setButtonBadResponse:nil];
    [self setButtonWarningResponse:nil];
    [self setButtonStartPauseRecording:nil];
    [self setTextGrouping:nil];
    [self setBarButtonRecordPauseAudio:nil];
    [self setImageSelectedFlag:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)viewDidAppear:(BOOL)animated 
{
    [super viewDidAppear:animated];
    
    // Set the recorded flag to false
    self.recorded = NO;
}


-(void)setupAudioRecorder
{
    IRAudioFileHelper *audioFileHelper = [[IRAudioFileHelper alloc]init];
    
    // Generate the audio file name
    self.response.audioFileName = [audioFileHelper getNewAudioFileName];
    NSString *soundFilePath = [audioFileHelper getFullPathForAudioFileName:self.response.audioFileName];
    
    // Create the audio recorder
    self.audioRecorder = [audioFileHelper createAudioRecorderWithFilename:soundFilePath];
    
    // Get ready to record
    [self.audioRecorder prepareToRecord];
    
    // Set the initial start time to 0
    self.currentQuestionStartTime = 0;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Should be portrait only on phone
    return [IRDeviceHelper shouldAutoRotatePortaitOnlyForPhone:interfaceOrientation];
}


-(void)moveToQuestionWithIndex:(NSInteger)newQuestionIndex
{
    // Save the current response
    [self saveCurrentQuestion];
    
    // End the audio segment
    [self endAudioSegment];
      
    // Set the new index
    self.questionIndex = newQuestionIndex;
       
    // Refresh the question display
    [self refreshQuestionDisplay];
}

-(void)endAudioSegment
{
    // Grab the current end time
    NSTimeInterval endTime = self.audioRecorder.currentTime;
    
    // If any audio was recorded then we need to store an audio segment
    if (endTime != 0 && endTime != self.currentQuestionStartTime)
    {
        // Ignore secments if it is less than 0.25 seconds
        if ((endTime - self.currentQuestionStartTime) > 0.25)
        {
            // Create the audio segment
            IRAudioResponseSegment *audioSegment = [[IRAudioResponseSegment alloc]initWithStartTime:self.currentQuestionStartTime endTime:endTime];
            // Add to the list for the current question
            IRQuestionResponse *question = [self.response.questions objectAtIndex:self.questionIndex];
            [question.audioSegments addObject:audioSegment];
        }
    }
    
    // Set the new start time to the current recorder time
    self.currentQuestionStartTime = self.audioRecorder.currentTime;
}

- (IBAction)previousQuetstion:(id)sender {
    // Move to the previous question
    [self moveToQuestionWithIndex:self.questionIndex - 1];
}


- (IBAction)nextQuestion:(id)sender {
    // Move to the next question
    [self moveToQuestionWithIndex:self.questionIndex + 1];
}

-(void)saveCurrentQuestion
{
    // Save the question response
    IRQuestionResponse *currentQuestion = [self.response.questions objectAtIndex:self.questionIndex];
    currentQuestion.response = textViewResponse.text;
    
    // Set the respondent name and grouping
    if (textRespondentName)
    {
        self.response.respondent = textRespondentName.text;
    }
    if(textGrouping)
    {
        self.response.grouping = textGrouping.text;
    }
}

-(void)complete
{
    [self performSegueWithIdentifier:@"CompleteInterview" sender:nil];
}

-(void)refreshQuestionDisplay
{
    // Ensure that there is at least one question
    [self checkQuestionsExist];
    
    // Get the current question
    IRQuestionResponse *currentQuestion = [self.response.questions objectAtIndex:self.questionIndex];
    
    // Update the labels and text view
    labelQuestionText.text = currentQuestion.question;
    textViewResponse.text = currentQuestion.response;
    labelQuestionNumber.text = [NSString stringWithFormat:@"Question %d of %d", self.questionIndex + 1, self.response.questions.count];
    
    // If this is the first question then hide the "Previous" button
    if (self.questionIndex == 0)
    {
        buttonPrevious.hidden = YES;
    }
    else {
        buttonPrevious.hidden = NO;
    }
    
    // If this is the last question then hide the "Next" button and change the text of the "Progress" button
    if (self.questionIndex == self.response.questions.count - 1)
    {
        if ([IRDeviceHelper isIpad])
        {
            [buttonProgress setTitle:@"Complete" forState:UIControlStateNormal];
        }
        else
        {
            [buttonProgress setTitle:@"Done" forState:UIControlStateNormal];
        }
        
        buttonNextQuestion.hidden = YES;
    }
    else
    {
        [buttonProgress setTitle:@"Next" forState:UIControlStateNormal];
        buttonNextQuestion.hidden = NO;
    }
    
    // Refresh the flags
    [self refreshFlagsDisplay];
}

-(void)refreshFlagsDisplay
{
    // Get the current question
    IRQuestionResponse *currentQuestion = [self.response.questions objectAtIndex:self.questionIndex];

    // Set the relevant alpha levels
    [self updateFlagsUiSettings:buttonGoodResponse set:[currentQuestion.flags containsObject:[IRResponseFlags good]]];
    [self updateFlagsUiSettings:buttonBadResponse set:[currentQuestion.flags containsObject:[IRResponseFlags bad]]];
    [self updateFlagsUiSettings:buttonWarningResponse set:[currentQuestion.flags containsObject:[IRResponseFlags warning]]];
    
    // Update the appropriate image
    if (currentQuestion.flags.count > 0)
    {
        // Get the first flag
        NSString *firstFlag = [currentQuestion.flags objectAtIndex:0];
        
        // Set the image
        NSString *flagImageName = [IRResponseFlags getImageNameForFlag:firstFlag];
        [self.imageSelectedFlag setImage:[UIImage imageNamed:flagImageName]];
    }
    else
    {
        // Set the image to be empty
        NSString *flagImageName = [IRResponseFlags getImageNameForFlag:[IRResponseFlags noFlag]];
        [self.imageSelectedFlag setImage:[UIImage imageNamed:flagImageName]];
    }
}

-(void)updateFlagsUiSettings:(UIButton*)flagButton set:(BOOL)set
{
    if (set)
    {
        // Flag is set so Alpha should be 100%
        flagButton.alpha = 1.0;
    }
    else 
    {
        // Flag is not set so Alpha is 20%
        flagButton.alpha = 0.2;
    }
}

-(void)checkQuestionsExist
{
    // Check for if there are no questions
    if (self.response.questions.count == 0)
    {
        // Insert a blank question
        IRQuestionResponse *questionResponse = [[IRQuestionResponse alloc]initWithQuestion:@"Empty Question"];
        [self.response.questions addObject:questionResponse];
        
        // Set the current index to 0
        self.questionIndex = 0;
        return;
    }
    
    // If the index is beyond the final question then push it back one
    if (self.questionIndex > self.response.questions.count - 1)
    {
        self.questionIndex = self.response.questions.count - 1;
    }
}

-(void)newQuestionViewControllerDidFinish:(IRNewQuestionViewController *)controller question:(NSString *)question
{
    // Save the current question being edited
    [self saveCurrentQuestion];
    
    // Save the question
    IRQuestionResponse *newQuestion = [[IRQuestionResponse alloc]initWithQuestion:question];
    [self.response.questions insertObject:newQuestion atIndex:self.questionIndex + 1];
    
    // Move to the next question
    [self moveToQuestionWithIndex:self.questionIndex + 1];
    
    // Dismiss the view controller
    [self.popoverController dismissPopoverAnimated:YES];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the destination view controller
    id destinationViewController = [segue destinationViewController];
    
    // Check for if this is a popover and if so - hold onto the popover controller so that we can dismiss later
    if ([segue isKindOfClass:[UIStoryboardPopoverSegue class]])
    {
        self.popoverController = ((UIStoryboardPopoverSegue*)segue).popoverController;
    }
    
    if ([[segue identifier] isEqualToString:@"NewQuestion"]) {
        IRNewQuestionViewController *newQuestionViewController = (IRNewQuestionViewController*)destinationViewController;
        newQuestionViewController.delegate = self;
    }
    
    if ([[segue identifier] isEqualToString:@"CompleteInterview"]) {
        IRCompleteInterviewViewController *newViewController = (IRCompleteInterviewViewController *)destinationViewController;
        
        // Set the delegate and the response
        newViewController.delegate = self;
        newViewController.response = self.response;
    }
    
    if ([[segue identifier] isEqualToString:@"SaveAsTemplate"]) {
        IRSaveAsTemplateViewController *newViewController = (IRSaveAsTemplateViewController *)destinationViewController;
        
        // Set the delegate and the response
        newViewController.delegate = self;
    }
    
    if ([[segue identifier] isEqualToString:@"SelectGrouping"]) {
        IRSelectGroupingViewController *newViewController = (IRSelectGroupingViewController *)destinationViewController;
        
        // Set the delegate and the response
        newViewController.delegate = self;
    }
    
    if ([[segue identifier] isEqualToString:@"SelectFlag"]) {
        IRSelectResponseFlagViewController *newViewController = (IRSelectResponseFlagViewController *)destinationViewController;
        
        // Set the delegate and the response
        newViewController.delegate = self;
    }
}



- (IBAction)deleteQuestion:(id)sender {
    // Delete the question
    [self.response.questions removeObjectAtIndex:self.questionIndex];
    
    // Refresh the question display
    [self refreshQuestionDisplay];
}

- (IBAction)buttonProgress:(id)sender {
    // Save the current response
    [self saveCurrentQuestion];
    
    // This is actually a complete
    if (self.questionIndex == self.response.questions.count - 1)
    {
        [self complete];
        return;
    }
    
    // Go to the next question
    [self nextQuestion:sender];
}

- (IBAction)cancel:(id)sender {
    // Show the alert view
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Cancel?" message:@"Are you sure you want to cancel this interview?" delegate:self cancelButtonTitle:@"No" otherButtonTitles: @"Yes", nil];
    [alertView show];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    // Cancel
    if (buttonIndex == 1)
    {
        // Stop recording
        [self.audioRecorder stop];
        self.audioRecorder = nil;

        // Delete the file
        IRAudioFileHelper *audioFileHelper = [[IRAudioFileHelper alloc]init];
        [audioFileHelper deleteAudioFileWithName:self.response.audioFileName];
        
        // Allow the application from dimming the screen / locking
        [[UIApplication sharedApplication]setIdleTimerDisabled:NO];
        
        // Call the delegate to update the view
        [self.delegate recordResponseViewControllerCancelled:self];
    }
}

- (IBAction)actionsButton:(id)sender {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Action" delegate:self cancelButtonTitle:@"Cancel"destructiveButtonTitle:@"Delete Question" otherButtonTitles:@"Add Question", @"Save as Template", nil];
    
    // Show the action sheet from the tool bar
    [actionSheet showFromBarButtonItem:sender animated:YES];
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        // Delete the questions
        [self deleteQuestion:nil];
    }
    
    if (buttonIndex == 1)
    {
        // Add a new question
        [self performSegueWithIdentifier:@"NewQuestion" sender:actionSheet];
    }
    
    if (buttonIndex == 2)
    {
        // Save as template
        [self performSegueWithIdentifier:@"SaveAsTemplate" sender:actionSheet];
    }
}

-(void)completeInterviewViewControllerDone:(IRCompleteInterviewViewController *)controller response:(IRResponse *)response
{
    // Update the response
    self.response = response;
    
    // Dismiss the popover
    [self.popoverController dismissPopoverAnimated:YES];
    
    // End the audio segment
    [self endAudioSegment];
    
    if (self.recorded && !self.audioRecorder.recording)
    {
        // Resume recording before stopping - otherwise for some reason file cannot be played
        [self.audioRecorder record];
    }
    
    // Stop recording
    [self.audioRecorder stop];
    
    // Check for if no audio was recorded
    if (!self.recorded)
    {
        // Delete the file
        IRAudioFileHelper *audioFileHelper = [[IRAudioFileHelper alloc]init];
        [audioFileHelper deleteAudioFileWithName:self.response.audioFileName];
        
        // Remove the audio file name
        self.response.audioFileName = nil;
    }
    
    // Allow the application from dimming the screen / locking
    [[UIApplication sharedApplication]setIdleTimerDisabled:NO];
    
    // Call the delegate
    [self.delegate recordResponseViewControllerComplete:self response:self.response];
}



-(void)saveAsTemplateControllerDone:(IRSaveAsTemplateViewController *)controller templateName:(NSString *)templateName
{
    // Create the template
    IRTemplate *template = [[IRTemplate alloc]initWitTemplateName:templateName];
    
    // Create the questions
    for(IRQuestionResponse *questionResponse in self.response.questions)
    {
        [template addQuestion:questionResponse.question];
    }
    
    
    // Save the template
    IRTemplateDataController *templateDataController = [[IRTemplateDataController alloc]init];
    [templateDataController storeTemplate:template];
    
    // Dismiss the popover
    [self.popoverController dismissPopoverAnimated:YES];
}


- (IBAction)goodResponsePressed:(id)sender {    
    // Set the flag
    [self setFlag:[IRResponseFlags good]];
}

-(void)setFlag:(NSString*) flag
{
    // Get the current question
    IRQuestionResponse *currentQuestion = [self.response.questions objectAtIndex:self.questionIndex];
    
    // If the flag is not set then set it else unset it
    if ([currentQuestion.flags containsObject:flag])
    {
        [currentQuestion.flags removeObject:flag];
    }
    else 
    {
        // Clear any existing flags
        [currentQuestion.flags removeAllObjects];
        
        // Add the new flag
        [currentQuestion.flags addObject:flag];
    }
    
    // Refresh the display
    [self refreshFlagsDisplay];
}
- (IBAction)badResponsePressed:(id)sender {
    // Set the flag
    [self setFlag:[IRResponseFlags bad]];

}

- (IBAction)warningResponsePressed:(id)sender {
    // Set the flag
    [self setFlag:[IRResponseFlags warning]];

}


- (IBAction)startPauseRecording:(id)sender {
    if (self.audioRecorder.recording)
    {
        // Currently recording
        // Pause the recording
        [self.audioRecorder pause];
        
        // Update button text
        [buttonStartPauseRecording setTitle: @"Resume" forState: UIControlStateNormal];
        [barButtonRecordPauseAudio setTitle:@"Resume"];
        
        // Allow the application from dimming the screen / locking
        [[UIApplication sharedApplication]setIdleTimerDisabled:NO];
    }
    else 
    {
        // Not started or paused
        // Resume the recording
        [self.audioRecorder record];
        
        // Set the recorded flag
        self.recorded = YES;
        
        // Update button text
        [buttonStartPauseRecording setTitle: @"Pause" forState: UIControlStateNormal];
        [barButtonRecordPauseAudio setTitle:@"Pause"];
        
        // Prevent the application from dimming the screen / locking
        [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
    }
}

- (IBAction)provideAppFeedback:(id)sender 
{
    // Show the UserVoice screen
    [IRUserVoiceHelper presentUserVoiceModalViewControllerForParent:self];
}

-(void)selectGroupingViewControllerDidFinish:(IRSelectGroupingViewController *)controller grouping:(NSString *)grouping
{
    // Update the grouping text
    textGrouping.text = grouping;
    
    // Dismiss the popover
    [self.popoverController dismissPopoverAnimated:YES];
}

-(void)selectResponseFlagViewControllerDidFinish:(IRSelectResponseFlagViewController *)controller flag:(NSString *)flag
{
    // Update the flag
    [self setFlag:flag];
    
    // Dismiss the controller
    [self.navigationController popViewControllerAnimated:YES];
}
@end
