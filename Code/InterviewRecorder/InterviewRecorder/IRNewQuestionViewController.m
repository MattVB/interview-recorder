//
//  IRNewQuestionViewController.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 14/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IRNewQuestionViewController.h"

@interface IRNewQuestionViewController ()

@end

@implementation IRNewQuestionViewController
@synthesize textQuestion;
@synthesize delegate = _delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Set the focus to the question field.
    [self.textQuestion becomeFirstResponder];
}

- (void)viewDidUnload
{
    [self setTextQuestion:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (IBAction)save:(id)sender {
    // Call the delegate
    [self.delegate newQuestionViewControllerDidFinish:self question:textQuestion.text];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // Release the focus and then save
    if (textField == self.textQuestion) {
        [textField resignFirstResponder];
        [self save:nil];
    }
    
    return YES;
}
@end
