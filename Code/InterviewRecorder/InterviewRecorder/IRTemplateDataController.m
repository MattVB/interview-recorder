//
//  IRTemplateDataController.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 15/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IRTemplateDataController.h"
#import "IRTemplate.h"

@implementation IRTemplateDataController

static NSString *templatesListKey = @"TemplatesList";

-(NSMutableArray*)getTemplates
{
    // Get the data
    NSUserDefaults *userSettings = [NSUserDefaults standardUserDefaults];
    NSData *data = [userSettings objectForKey:templatesListKey];
    
    // If no templates then create the default templates and save
    if (!data)
    {
        [self setTemplates:[self createDefaultTemplates]];
        data = [userSettings objectForKey:templatesListKey];
    }
    
    // Unarchive the templates and return
    NSArray *arr = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return [[NSMutableArray alloc]initWithArray:arr];
}

-(void)storeTemplate:(IRTemplate *)template
{
    // Get the list
    NSMutableArray *array = [self getTemplates];
    
    // Check for if this is an edit
    IRTemplate *replacesTemplate = [self getTemplateFromArray:array withName:template.templateName];
    
    if(replacesTemplate)
    {
        // This is an edit
        // Get the index of the item
        NSInteger index = [array indexOfObject:replacesTemplate];
        
        // Replace the old with the new
        [array replaceObjectAtIndex:index withObject:template];
    }
    else 
    {
        // Is an insert - so add to the list
        [array addObject:template];
    }
    
    // Store the updated list
    [self setTemplates:array];
}

-(NSMutableArray*)createDefaultTemplates
{
    NSMutableArray *result = [[NSMutableArray alloc]init];
    
    // Create Age/Sex/Location templates
    IRTemplate *template = [[IRTemplate alloc]initWitTemplateName:@"Age / Sex / Location"];
    [template addQuestion:@"Age?"];
    [template addQuestion:@"Sex?"];
    [template addQuestion:@"Location?"];
    [result addObject:template];
    
    // Create a basic job interview template
    template = [[IRTemplate alloc]initWitTemplateName:@"Basic Job Interview"];
    [template addQuestion:@"Tell me about yourself."];
    [template addQuestion:@"What are your strengths?"];
    [template addQuestion:@"What are your weaknesses?"];
    [template addQuestion:@"Why do you want this job?"];
    [template addQuestion:@"Where would you like to be in your career five years from now?"];
    [template addQuestion:@"What attracted you to this company?"];
    [template addQuestion:@"What can you do for us that other candidates can't? "];
    [template addQuestion:@"What were the responsibilities of your last position?  "];
    [template addQuestion:@"What do you know about our company? "];
    [template addQuestion:@"Do you have any questions for me?"];
    [result addObject:template];
    
    
    return result;
}

-(void)deleteTemplateWithName:(NSString *)name
{
    // Get the templates
    NSMutableArray *templates = [self getTemplates];
    
    // Get the template to remove
    IRTemplate *templateToDelete = [self getTemplateFromArray:templates withName:name];
    
    // Delete the template if found
    if (templateToDelete)
    {
        [templates removeObject:templateToDelete];
    }
    
    // Store the updated list
    [self setTemplates:templates];
}

-(BOOL)hasTemplateWithName:(NSString *)name
{
    // Get the list of templates
    NSMutableArray *array = [self getTemplates];
    
    // Try to find the template with the name
    IRTemplate *template = [self getTemplateFromArray:array withName:name];
    
    if (template)
    {
        return YES;
    }
    
    return NO;
}


-(void)setTemplates:(NSMutableArray*)templates
{
    // Archive the data
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:templates];
    
    // Store to the UserDefaults
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:templatesListKey];
}

-(IRTemplate*)getTemplateFromArray:(NSMutableArray*)array withName:(NSString*)name
{
    for(IRTemplate *template in array)
    {
        if ([template.templateName isEqualToString:name])
        {
            return template;
        }
    }
    
    return nil;
}

@end
