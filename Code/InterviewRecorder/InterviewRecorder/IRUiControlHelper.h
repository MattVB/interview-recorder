//
//  IRUiControlHelper.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 29/07/12.
//
//

#import <Foundation/Foundation.h>

@interface IRUiControlHelper : NSObject

+(void)formatTextView:(UITextView*)textView;

@end
