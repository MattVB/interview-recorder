//
//  IREditTemplateViewController.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 18/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IRTemplate.h"
#import "IRAddQuestionsViewController.h"
#import "IRConfirmSaveTemplateViewController.h"

@protocol IREditTemplateViewControllerDelegate;

@interface IREditTemplateViewController : UITableViewController <IRAddQuestionsViewControllerDelegate, IRConfirmSaveTemplateViewControllerDelegate>

    @property (strong, nonatomic) IRTemplate *template;
- (IBAction)cancel:(id)sender;

@property (weak, nonatomic) id <IREditTemplateViewControllerDelegate> delegate;

@end


@protocol IREditTemplateViewControllerDelegate <NSObject>

-(void)editTemplateControllerDone:(IREditTemplateViewController*)controller;

@end