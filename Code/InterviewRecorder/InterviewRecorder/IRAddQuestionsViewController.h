//
//  IRAddQuestionsViewController.h
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 29/07/12.
//
//

#import <UIKit/UIKit.h>

@protocol IRAddQuestionsViewControllerDelegate;

@interface IRAddQuestionsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *textViewQuestions;
- (IBAction)saveQuestions:(id)sender;



@property (weak, nonatomic) id <IRAddQuestionsViewControllerDelegate> delegate;

@end


@protocol IRAddQuestionsViewControllerDelegate <NSObject>
- (void)addQuestionsViewControllerDidFinish:(IRAddQuestionsViewController*)controller questions:(NSMutableArray *)questions;
@end
