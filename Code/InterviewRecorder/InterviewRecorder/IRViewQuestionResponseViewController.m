//
//  IRViewQuestionResponseViewController.m
//  InterviewRecorder
//
//  Created by Matthew van Boheemen on 2/08/12.
//
//

#import "IRViewQuestionResponseViewController.h"
#import "IRUiControlHelper.h"

@interface IRViewQuestionResponseViewController ()

@end

@implementation IRViewQuestionResponseViewController

@synthesize response = _response;
@synthesize questionResponse = _questionResponse;
@synthesize textViewResponse = _textViewResponse;
@synthesize buttonPlayAudio = _buttonPlayAudio;
@synthesize audioPlayer = _audioPlayer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [IRUiControlHelper formatTextView:self.textViewResponse];
}

- (void)viewDidUnload
{
    [self setTextViewResponse:nil];
    [self setButtonPlayAudio:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Set the label values
    self.textViewResponse.text = [self.questionResponse getFormattedResponse];
    
    // Hide the play audio button if there is no audio for this question
    if(self.questionResponse.audioSegments.count == 0)
    {
        self.buttonPlayAudio.hidden = YES;
    }
    else
    {
        self.buttonPlayAudio.hidden = NO;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (IBAction)playAudio:(id)sender
{
    // Tell the play to play the audio
    [self.audioPlayer playQuestionResponse:self.questionResponse];
}
@end
